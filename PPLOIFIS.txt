       IDENTIFICATION DIVISION.
       PROGRAM-ID.    PPLOIFIS.

      *REMARKS.
      *    THIS CALLED PROGRAM WILL TRANSLATE ACCOUNT FUND
      *    PROFILE TO IFIS CHART OF ACCOUNTS BASED ON PARAMETERS
      *    PASSED FROM THE CALLING PROGRAM.  THE PARAMETERS
      *    ARE:  LOCATION, ACCOUNT, FUND, SUB, OBJECT-CODE.
      *
      *    THE FOLLOWING DATA WILL BE RETURNED TO THE CALLING
      *    PROGRAM:  INDEX, FUND, ORGANIZATION, ACCOUNT, PROGRAM
      *    ACTIVITY, LOCATION.

       AUTHOR.  J.E. MCCLANE.
       DATE-WRITTEN.
       DATE-COMPILED.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT OLD2IFIS-FILE        ASSIGN TO UT-OLD2IFIS
               ACCESS IS DYNAMIC
               ORGANIZATION IS INDEXED
               RECORD KEY IS OLD2IFIS-VSAM-KEY
               FILE STATUS IS VSAM-FILE-STATUS.

       DATA DIVISION.
       FILE SECTION.

      ****************************************************************
      *  IFIS TRANSLATION FILE                                       *
      *  THIS FILE WILL BE DEFINED AS A VSAM FILE                    *
      ****************************************************************
       FD  OLD2IFIS-FILE
           LABEL RECORDS ARE STANDARD.

       01  OLD2IFIS-RECORD.
           03  OLD2IFIS-VSAM-KEY.
               05  OLD2IFIS-OLD-TYPE    PIC X(02).
               05  OLD2IFIS-OLD-VALUE   PIC X(17).
           03  OLD2IFIS-VALUES.
               05  OLD2IFIS-INDEX       PIC X(10).
               05  OLD2IFIS-FUND        PIC X(06).
               05  OLD2IFIS-ORG         PIC X(06).
               05  OLD2IFIS-ACCOUNT     PIC X(06).
               05  OLD2IFIS-PROG        PIC X(06).
               05  OLD2IFIS-ACTIVITY    PIC X(06).
               05  OLD2IFIS-LOC         PIC X(06).
           EJECT
       WORKING-STORAGE SECTION.

      ****************************************************************
      *  HOLD AREA FOR OLD2IFIS RECORD                               *
      ****************************************************************
       01  WS-OLD2IFIS-RECORD.
           03  WS-OLD2IFIS-KEY.
               05  WS-OLD2IFIS-OLD-TYPE     PIC X(02) VALUE SPACES.
               05  WS-OLD2IFIS-OLD-VALUE    PIC X(17) VALUE SPACES.
           03  WS-OLD2IFIS-VALUES.
               05  WS-OLD2IFIS-INDEX        PIC X(10) VALUE SPACES.
               05  WS-OLD2IFIS-FUND         PIC X(06) VALUE SPACES.
               05  WS-OLD2IFIS-ORG          PIC X(06) VALUE SPACES.
               05  WS-OLD2IFIS-ACCOUNT      PIC X(06) VALUE SPACES.
               05  WS-OLD2IFIS-PROG         PIC X(06) VALUE SPACES.
               05  WS-OLD2IFIS-ACTIVITY     PIC X(06) VALUE SPACES.
               05  WS-OLD2IFIS-LOC          PIC X(06) VALUE SPACES.

      ****************************************************************
      *  VSAM FILE STATUS FIELD                                      *
      ****************************************************************
       01  VSAM-FILE-STATUS                 PIC X(02) VALUE SPACES.
           88  VSAM-OK                      VALUE '00'.
           88  VSAM-REC-NOT-FND             VALUE '23'.
           88  VSAM-FILE-ERROR              VALUE '01' THRU '22'
                                                  '24' THRU '99'.
       01  WS-VALUES.
           03  LOCATION                PIC X     VALUE SPACES.
               88  VALID-LOCATION VALUE '6' 'O'.
           03  ACCOUNT                 PIC 9(06) VALUE 0.
               88  BAL-SHEET-ACCOUNT   VALUE 100000 THRU 199999.
               88  FUND-BAL-ACCOUNT    VALUE 100300 THRU 101599,
                                             101700 THRU 101799,
                                             102200 THRU 103999,
                                             104100 THRU 109599,
                                             119700 THRU 119900.
               88  REVENUE-ACCOUNT     VALUE 200000 THRU 299999.
               88  EXPENDITURE-ACCOUNT VALUE 400000 THRU 899999.
               88  PLANT-ACCOUNT       VALUE 900000 THRU 999990.
           03  FUND                    PIC X(05) VALUE SPACES.
           03  FUND-NBR REDEFINES FUND PIC 9(05).
           03  SUB                     PIC X(01) VALUE SPACES.
           03  OBJECT-CODE.
               05  OBJECT-CD           PIC X(02) VALUE SPACES.
               05  SERVICE-CD          PIC X(02) VALUE SPACES.
       EJECT
       LINKAGE SECTION.

       01  PARAMETER-ARRAY.
           03  OLD-CHART-INFO.
               05  WS-OLD-LOCATION          PIC X.
               05  WS-OLD-ACCOUNT           PIC X(6).
               05  WS-OLD-FUND              PIC X(5).
               05  WS-OLD-SUB               PIC X.
               05  WS-OLD-OBJECT-CODE       PIC X(4).
           03  NEW-IFIS-CHART-INFO.
               05  WS-NEW2IFIS-INDEX        PIC X(10).
               05  WS-NEW2IFIS-FUND         PIC X(6).
               05  WS-NEW2IFIS-ORG          PIC X(6).
               05  WS-NEW2IFIS-ACCOUNT      PIC X(6).
               05  WS-NEW2IFIS-PROG         PIC X(6).
               05  WS-NEW2IFIS-ACTIVITY     PIC X(6).
               05  WS-NEW2IFIS-LOCATION     PIC X(6).
           03  STATUS-RETURN-CODE           PIC XX.
       EJECT
       PROCEDURE DIVISION USING PARAMETER-ARRAY.
       0000-MAIN SECTION.
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL CONTROL THIS PROGRAM.          *
      *       INITIALIZE WORKING STORAGE, OPEN FILES, READ PARAMETER *
      *       PERFORM ANY END OF PROCESSING CLEANUP DUTIES           *
      ****************************************************************
       0000-MAINLINE.

           IF OLD-CHART-INFO = 'OPEN             '
               PERFORM 0150-OPEN-OLD2IFIS
           ELSE
               IF OLD-CHART-INFO = 'CLOSE            '
                   PERFORM 9100-CLOSE-OLD2IFIS
               ELSE
                   PERFORM 0100-INIT
                   PERFORM 0250-CHECK-PROCESS-DATA.
           GOBACK.
           EJECT
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL OPEN FILES AND INITIALIZE      *
      *  WORKING STORAGE.                                            *
      ****************************************************************
       0100-INIT SECTION.

           MOVE SPACES TO OLD2IFIS-RECORD.
           MOVE SPACES TO WS-OLD2IFIS-RECORD.
           MOVE SPACES TO NEW-IFIS-CHART-INFO.
           MOVE ZEROS  TO STATUS-RETURN-CODE.

       0100-INIT-EXIT.
           EXIT.



       0150-OPEN-OLD2IFIS SECTION.

           OPEN INPUT OLD2IFIS-FILE.
           IF VSAM-FILE-STATUS NOT = ZEROS
              DISPLAY 'OPEN ERROR ON CALLED VSAM FILE'
              MOVE '98' TO STATUS-RETURN-CODE.

       0150-OPEN-OLD2IFIS-EXIT.
           EXIT.
       EJECT
       0250-CHECK-PROCESS-DATA SECTION.
      ****************************************************************
      * THIS PARAGRAPH WILL PROCESS EACH INPUT TRANSACTION.          *
      * THE OLD2IFIS VALUES WILL BE TRANSLATED.                      *
      ****************************************************************

           MOVE WS-OLD-LOCATION    TO LOCATION.
           MOVE WS-OLD-ACCOUNT     TO ACCOUNT.
           MOVE WS-OLD-FUND        TO FUND.
           MOVE WS-OLD-SUB         TO SUB.
           MOVE WS-OLD-OBJECT-CODE TO OBJECT-CODE.

           IF VALID-LOCATION
               NEXT SENTENCE
           ELSE
               MOVE '05' TO STATUS-RETURN-CODE.

           IF BAL-SHEET-ACCOUNT OR FUND-BAL-ACCOUNT OR
               REVENUE-ACCOUNT OR EXPENDITURE-ACCOUNT OR
               PLANT-ACCOUNT
                  NEXT SENTENCE
           ELSE
              MOVE '15' TO STATUS-RETURN-CODE.

           IF FUND = SPACES
               MOVE '25' TO STATUS-RETURN-CODE.

           IF ACCOUNT = SPACES
               MOVE '35' TO STATUS-RETURN-CODE.

           IF SUB = SPACES
               MOVE '45' TO STATUS-RETURN-CODE.

           IF STATUS-RETURN-CODE = ZEROS
               PERFORM 0270-TRANSLATE-AFP.

       0250-CHECK-PROCESS-DATA-EXIT.
           EXIT.
       EJECT
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL TRANSLATE OLD AFP VALUES TO    *
      *  NEW CHART OF ACCOUNT VALUES.                                *
      ****************************************************************
       0270-TRANSLATE-AFP SECTION.

           IF EXPENDITURE-ACCOUNT
               MOVE 'AF'   TO OLD2IFIS-OLD-TYPE
               MOVE SPACES TO OLD2IFIS-OLD-VALUE
               STRING LOCATION DELIMITED BY SIZE
                      ACCOUNT  DELIMITED BY SIZE
                      FUND     DELIMITED BY SIZE
                 INTO OLD2IFIS-OLD-VALUE
               PERFORM 0275-READ-OLD2IFIS.

           IF WS-OLD2IFIS-VALUES = SPACES OR LOW-VALUES
               MOVE '01' TO STATUS-RETURN-CODE.

           MOVE WS-OLD2IFIS-VALUES TO NEW-IFIS-CHART-INFO.

       0270-TRANSLATE-AFP-EXIT.
           EXIT.
       EJECT
      ****************************************************************
      *  THE FOLLOWING PARAGRAPH WILL READ THE OLD2IFIS FILE AND     *
      *  TRANSLATE THE AFP VALUE(S) TO THE NEW COA VALUE(S)          *
      ****************************************************************
       0275-READ-OLD2IFIS SECTION.

           READ OLD2IFIS-FILE.
           IF VSAM-FILE-ERROR
               DISPLAY 'READ ERROR ON CALLED VSAM FILE'
               MOVE '99' TO STATUS-RETURN-CODE.

           IF OLD2IFIS-INDEX NOT = SPACES AND
              WS-OLD2IFIS-INDEX  = SPACES
               MOVE OLD2IFIS-INDEX TO WS-OLD2IFIS-INDEX.

           IF OLD2IFIS-FUND NOT = SPACES AND
              WS-OLD2IFIS-FUND  = SPACES
               MOVE OLD2IFIS-FUND TO WS-OLD2IFIS-FUND.

           IF OLD2IFIS-ORG NOT = SPACES AND
              WS-OLD2IFIS-ORG  = SPACES
               MOVE OLD2IFIS-ORG TO WS-OLD2IFIS-ORG.

           IF OLD2IFIS-ACCOUNT NOT = SPACES AND
              WS-OLD2IFIS-ACCOUNT  = SPACES
               MOVE OLD2IFIS-ACCOUNT TO WS-OLD2IFIS-ACCOUNT.

           IF OLD2IFIS-PROG NOT = SPACES AND
              WS-OLD2IFIS-PROG  = SPACES
               MOVE OLD2IFIS-PROG TO WS-OLD2IFIS-PROG.

           IF OLD2IFIS-ACTIVITY NOT = SPACES AND
              WS-OLD2IFIS-ACTIVITY  = SPACES
               MOVE OLD2IFIS-ACTIVITY TO WS-OLD2IFIS-ACTIVITY.

           IF OLD2IFIS-LOC NOT = SPACES AND
              WS-OLD2IFIS-LOC  = SPACES
               MOVE OLD2IFIS-LOC TO WS-OLD2IFIS-LOC.

           MOVE SPACES TO OLD2IFIS-RECORD.

       0275-READ-OLD2IFIS-EXIT.
           EXIT.
       EJECT
       9100-CLOSE-OLD2IFIS SECTION.

           CLOSE OLD2IFIS-FILE.

           IF VSAM-FILE-STATUS NOT EQUAL TO ZEROS
              DISPLAY 'CLOSE ERROR ON CALLED VSAM FILE'
              DISPLAY 'VSAM STATUS CODE = ' VSAM-FILE-STATUS.

       9100-CLOSE-OLD2IFIS-EXIT.
           EXIT.
