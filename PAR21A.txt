000100 IDENTIFICATION DIVISION.                                         PAR21A
000200 PROGRAM-ID.  PAR21A.                                             PAR21A
000300 DATE-WRITTEN.  APRIL 24, 1980.                                   PAR21A
000400 DATE-COMPILED.                                                   PAR21A
000500*REMARKS.                                                         PAR21A
000620******************************************************************PAR21A
000700*                                                                *PAR21A
000800* FUNCTION:                                                      *PAR21A
000900*    THIS PROGRAM READS ALL OF THE TABLE FILES USED BY THE PAR   *PAR21A
001000*    EXTRACTION PROCESS AND PLACES THEM INTO IN-CORE TABLES.     *PAR21A
001100*    ALL TABLES ARE SORTED INTO ASCENDING SEQUENCE, AND THE      *PAR21A
001200*    UNUSED TABLE ENTRIES PADDED.                                *PAR21A
001300*    THE TABLES PROCESSED BY THIS PROGRAM ARE:                   *PAR21A
001500*        EXCLUDED FEDERAL FUNDS TABLE - UNIQUE ENTRIES FOR CAMPUS*PAR21A
001600*        NINE MONTH FACULTY TITLE CODES - SAME FOR ALL CAMPUSES   PAR21A
001700*                                                                *PAR21A
001800* INPUT FILES:                                                   *PAR21A
002000*    PARI02EF - EXCLUDED FEDERAL FUND TABLE                      *PAR21A
002400*    PARI02NM - NINE MONTH FACULTY TITLE CODE TABLE              *PAR21A
002200*                                                                *PAR21A
002300* OUTPUT FILES:                                                  *PAR21A
002400*    NONE                                                        *PAR21A
002500*                                                                *PAR21A
002600* PARM:                                                          *PAR21A
002700*    NONE                                                        *PAR21A
002800*                                                                *PAR21A
002900* CALLED MODULES:                                                *PAR21A
003000*    NONE                                                        *PAR21A
003100*                                                                *PAR21A
003200* CALLING MODULES:                                               *PAR21A
003900*    PAR0200A - PAR  EXTRACT CONTROL                             *PAR21A
003400*                                                                *PAR21A
003500* RETURN CODES:                                                  *PAR21A
003600*    0000 - REQUESTED FUNCTION COMPLETED                         *PAR21A
003700*    0016 - ABNORMAL CONDITION ENCOUNTERED.  SEE DISPLAYS.       *PAR21A
003800*                                                                *PAR21A
003900* MODIFICATIONS:                                                 *PAR21A
004000*    DATE     WHO        MODIFICATION DESCRIPTION                *PAR21A
004100*  05/20/82   TMF    CHANGE TO ACCESS TITLE DATA FROM EDB     XX *PAR21A
004200*                    TABLE (TTL) REPLACEMENT PAYROLL SYSTEM  XXX *PAR21A
004400*                                                                *PAR21A
004500******************************************************************PAR21A
005100/                                                                 PAR21A
004700 ENVIRONMENT DIVISION.                                            PAR21A
004800 INPUT-OUTPUT SECTION.                                            PAR21A
004900 FILE-CONTROL.                                                    PAR21A
005500*    SELECT TITLE-CODE-PROFILE    ASSIGN TO DA-I-PARI02TC         PAR21A
005600*                                 ACCESS MODE IS SEQUENTIAL       PAR21A
005700*                                 NOMINAL KEY IS KEY-TTL-WK       PAR21A
005800*                                 RECORD KEY IS XTTL-KEY.         PAR21A
005600     SELECT EXCLUDED-FED-FUND     ASSIGN TO UT-S-PARI02EF.        PAR21A
006000     SELECT NINE-MONTH-CDS        ASSIGN TO UT-S-PARI02NM.        PAR21A
006100*    SELECT EXCLUDED-DEPT         ASSIGN TO UT-S-PARI02ED.        PAR21A
006200*    SELECT EXCLUDED-TITLE-CODES  ASSIGN TO UT-S-PARI02ET.        PAR21A
006300*    SELECT PRINCIPAL-INVESTR     ASSIGN TO UT-S-PARI02PI.        PAR21A
007800/                                                                 PAR21A
006000 DATA DIVISION.                                                   PAR21A
006100 FILE SECTION.                                                    PAR21A
008100*FD  TITLE-CODE-PROFILE           RECORDING MODE IS F             PAR21A
008200*                                 RECORD CONTAINS 224 CHARACTERS  PAR21A
008300*                                 BLOCK CONTAINS 0 RECORDS        PAR21A
008400*                                 LABEL RECORDS ARE STANDARD      PAR21A
008500*                                 DATA RECORD IS XTTL-RECORD.     PAR21A
007000 FD  EXCLUDED-FED-FUND            RECORDING MODE IS F             PAR21A
007100                                  RECORD CONTAINS 80 CHARACTERS   PAR21A
007200                                  BLOCK CONTAINS 0 RECORDS        PAR21A
007300                                  LABEL RECORDS ARE STANDARD      PAR21A
007400                                  DATA RECORD IS                  PAR21A
007500                                     EXCLD-FED-FUND-EFR.          PAR21A
010200*                                                                 PAR21A
007600 01  EXCLD-FED-FUND-EFR.                                          PAR21A
007800     05  CAMPUS-CODE-EFR          PIC 99.                         PAR21A
007900     05  FUND-LOW-EFR             PIC 9(5).                       PAR21A
008000     05  FUND-HIGH-EFR            PIC 9(5).                       PAR21A
008100     05  FUND-DESC-EFR            PIC X(15).                      PAR21A
008200     05  FILLER                   PIC X(53).                      PAR21A
010900/                                                                 PAR21A
011000 FD  NINE-MONTH-CDS               RECORDING MODE IS F             PAR21A
008500                                  RECORD CONTAINS 80 CHARACTERS   PAR21A
008600                                  BLOCK CONTAINS 0 RECORDS        PAR21A
008700                                  LABEL RECORDS ARE STANDARD      PAR21A
008800                                  DATA RECORD IS                  PAR21A
011500                                     NINE-MONTH-FACULTY-NIN.      PAR21A
011900*                                                                 PAR21A
012000 01  NINE-MONTH-FACULTY-NIN.                                      PAR21A
012100     05  TITLE-CODE-NIN           PIC 9(4).                       PAR21A
012200     05  TITLE-CODE-DESC-NIN      PIC X(30).                      PAR21A
012300     05  FILLER                   PIC X(46).                      PAR21A
012400/                                                                 PAR21A
012500*FD  EXCLUDED-DEPT                RECORDING MODE IS F             PAR21A
012600*                                 RECORD CONTAINS 80 CHARACTERS   PAR21A
012700*                                 BLOCK CONTAINS 0 RECORDS        PAR21A
012800*                                 LABEL RECORDS ARE STANDARD      PAR21A
012900*                                 DATA RECORD IS                  PAR21A
013000*                                    EXCLUDED-ACCTS-RECORD.       PAR21A
016600/                                                                 PAR21A
009500 WORKING-STORAGE SECTION.                                         PAR21A
009700 01  CONSTANTS-C.                                                 PAR21A
016900     05  MOD-NAME-C               PIC X(8) VALUE 'PAR0201A'.      PAR21A
009900     05  TTL-CONSTANT-C           PIC X(7) VALUE ' TTL   '.       PAR21A
010200 01  SWITCHES-SW.                                                 PAR21A
010300     05  TITL-CD-PRFL-EOF-SW      PIC X VALUE '0'.                PAR21A
010400         88  TITL-CD-PRFL-EOF     VALUE '1'.                      PAR21A
010500     05  EXCLD-FED-FUND-EOF-SW    PIC X VALUE '0'.                PAR21A
010600         88  EXCLD-FED-FUND-EOF   VALUE '1'.                      PAR21A
017600     05  NINE-MONTH-EOF-SW        PIC X VALUE '0'.                PAR21A
017700         88  NINE-MONTH-EOF       VALUE '1'.                      PAR21A
017800     05  EXCLD-DEPT-EOF-SW        PIC X VALUE '0'.                PAR21A
017900         88  EXCLD-DEPT-EOF       VALUE '1'.                      PAR21A
018000     05  EXCLD-TITL-CD-EOF-SW     PIC X VALUE '0'.                PAR21A
018100         88  EXCLD-TITL-CD-EOF    VALUE '1'.                      PAR21A
010700     05  PRINC-INVEST-EOF-SW      PIC X VALUE '0'.                PAR21A
010800         88  PRINC-INVEST-EOF     VALUE '1'.                      PAR21A
010900     05  PROCESS-COMPLETE-SW      PIC X VALUE '0'.                PAR21A
011000         88  PROCESS-COMPLETE     VALUE '1'.                      PAR21A
011200 01  WORK-AREAS-WK.                                               PAR21A
011300     05  KEY-TTL-WK               PIC X(13) VALUE ' TTL         '.PAR21A
011400     05  NUM-TITL-CD-PRFL-WK      PIC S9(4) COMP SYNC VALUE +0.   PAR21A
018900     05  NUM-EXCL-TITL-CD-WK      PIC S9(4) COMP SYNC VALUE +0.   PAR21A
011500     05  NUM-EXCL-FED-FUND-WK     PIC S9(4) COMP SYNC VALUE +0.   PAR21A
019100     05  NUM-EXCL-DEPTS-WK        PIC S9(4) COMP SYNC VALUE +0.   PAR21A
011600     05  NUM-PI-RCDS-WK           PIC S9(4) COMP SYNC VALUE +0.   PAR21A
019300     05  NUM-NINE-MO-WK           PIC S9(4) COMP SYNC VALUE +0.   PAR21A
011700     05  LAST-TITLE-CODE-WK       PIC 9(4) VALUE ZERO.            PAR21A
019500     05  LAST-EXCL-TITL-CODE-WK   PIC 9(4) VALUE ZERO.            PAR21A
019600     05  LAST-NINE-MONTH-CODE-WK  PIC 9(4) VALUE ZERO.            PAR21A
019700     05  HOLD-2DIG-ENT-WK         PIC XX.                         PAR21A
019800     05  HOLD-6DIG-ENT-WK         PIC X(7).                       PAR21A
011800     05  HOLD-PI-KEY-WK           PIC 9(9) VALUE ZERO.            PAR21A
020000     05  ACCT-NUM-WK.                                             PAR21A
020100         10  ACCT-SYS-WIDE-WK     PIC X.                          PAR21A
020200         10  ACCT-6DIG-WK         PIC X(6).                       PAR21A
020300/                                                                 PAR21A
012000 LINKAGE SECTION.                                                 PAR21A
020500 01  MODULE-COMMON-AREA.          COPY 'PARYMODC'.                PAR21A
023700*01  EXCLUDED-FEDERAL-FUNDS-EFF.  COPY 'PARYEFF'.                 PAR21A
013100******************************************************************PAR21A
023900*                                                                *PAR21A
024000*                P A R Y E F F                                   *PAR21A
024100*                                                                *PAR21A
024200*    THIS MEMBER DEFINES THE INTERNAL PROGRAM TABLE WHERE THE    *PAR21A
024300*    EXCLUDED FEDERAL FUND TABLE IS BROUGHT INTO CORE FOR USE.   *PAR21A
024400*    A DIFFERENT SET OF FUNDS ARE EXCLUDED FOR EACH CAMPUS.      *PAR21A
024500*    THE FUND NUMBERS IN THIS TABLE ARE WITHIN THE RANGE OF      *PAR21A
024600*    21100 TO 39999 BUT ARE NOT TO BE CONSIDERED FEDERAL FUNDS,  *PAR21A
024700*    (DIRECT CHARGE NUMBERS FOR GRANTS).                         *PAR21A
024800*                                                                *PAR21A
024900******************************************************************PAR21A
025000*                                                                 PAR21A
025100 01  EXCLUDED-FEDERAL-FUNDS-EFF.                                  PAR21A
025200*                                                                 PAR21A
025300*    05  MAX-NUM-ENTRIES-EFF      PIC S9(4) COMP SYNC VALUE +30.  PAR21A
025400     05  MAX-NUM-ENTRIES-EFF      PIC S9(4) COMP.                 PAR21A
025500     05  NUM-ENTRIES-EFF          PIC S9(4) COMP SYNC.            PAR21A
025600     05  TABLE-ENTRY-EFF          OCCURS 30 TIMES                 PAR21A
025700                                  INDEXED BY EFF-IX1              PAR21A
025800                                             EFF-IX2              PAR21A
025900                                             EFF-IX3.             PAR21A
026000         10  EXC-FUND-LOW-EFF     PIC 9(5).                       PAR21A
026100         10  EXC-FUND-HIGH-EFF    PIC 9(5).                       PAR21A
034200*01  NINE-MONTH-FACULTY-NM.       COPY 'PARYNINE'.                PAR21A
034300******************************************************************PAR21A
034400*                                                                *PAR21A
034500*                P A R Y N I N E                                 *PAR21A
034600*                                                                *PAR21A
034700*    THIS MEMBER DEFINES THE INTERNAL PROGRAM TABLE WHERE THE    *PAR21A
034800*    TITLE CODES THAT ARE TO BE CONSIDERED NINE MONTH            *PAR21A
034900*    FACULTY ARE TABLED.  THIS MEMBER IS USED BY THE PAR         *PAR21A
035000*    EXTRACTION PROCESS(PAR0200A).                               *PAR21A
035100*                                                                *PAR21A
035200******************************************************************PAR21A
035300 01  NINE-MONTH-FACULTY-NM.                                       PAR21A
035400*    05  MAX-ENTRS-NM             PIC S9(4) COMP SYNC VALUE +100. PAR21A
035500     05  MAX-ENTRS-NM             PIC S9(4) COMP.                 PAR21A
035600     05  NUM-ENTRS-NM             PIC S9(4) COMP SYNC.            PAR21A
035700     05  MAX-CODE-NM              PIC 9(4).                       PAR21A
035800     05  MIN-CODE-NM              PIC 9(4).                       PAR21A
035900     05  NINE-MONTH-ENTRS-NM      OCCURS 100 TIMES                PAR21A
036000                                  INDEXED BY NIN-IX1              PAR21A
036100                                             NIN-IX2              PAR21A
036200                                             NIN-IX3.             PAR21A
036300         10  NINE-MON-CD-NM       PIC 9(4).                       PAR21A
036400/                                                                 PAR21A
036500******************************************************************PAR21A
013200*    THE PROCEDURE DIVISION IS ORGANIZED INTO 4 MAIN SECTIONS.   *PAR21A
013300*    0000-MAINLINE  ENTRY POINT OF PROGRAM.  CHECKS FUNCTION     *PAR21A
013400*        CODE IN LINKAGE AND PERFORMS PROPER SECTION.            *PAR21A
013500*    1000-INITIALIZATION  INITIALIZED MODULE AND LOADS THE TABLES*PAR21A
013600*        THAT ARE CAMPUS INDEPENDENT.                            *PAR21A
013700*    4000-PROCESS  LOADS CAMPUS DEPENDENT TABLES BASED ON CAMPUS *PAR21A
013800*        CODE IN LINKAGE.                                        *PAR21A
013900*    9000-TERMINATION  CLOSES ALL FILES AND FINALIZES PROCESSING.*PAR21A
014000******************************************************************PAR21A
014200 PROCEDURE DIVISION USING MODULE-COMMON-AREA                      PAR21A
014400                          EXCLUDED-FEDERAL-FUNDS-EFF              PAR21A
038100                          NINE-MONTH-FACULTY-NM.                  PAR21A
014600 0000-MAINLINE SECTION.                                           PAR21A
038300 0000-MAIN-PARA.                                                  PAR21A
014800     IF INITIALIZATION                                            PAR21A
014900         PERFORM 1000-INITIALIZATION                              PAR21A
015000     ELSE                                                         PAR21A
015100         IF PROCESS                                               PAR21A
015200             PERFORM 4000-PROCESS                                 PAR21A
015300         ELSE                                                     PAR21A
015400             IF TERMINATION                                       PAR21A
015500                 PERFORM 9000-TERMINATION                         PAR21A
015600             ELSE                                                 PAR21A
015700                 DISPLAY MOD-NAME-C ' INVALID TYPE OF CALL OF '   PAR21A
015800                         TYPE-OF-CALL-LS                          PAR21A
015900                 GO TO 0010-SET-BAD-RETURN.                       PAR21A
016100     GO TO 0020-SET-GOOD-RETURN.                                  PAR21A
016300 0010-SET-BAD-RETURN.                                             PAR21A
016400     MOVE +16 TO RETURN-CODE.                                     PAR21A
016500     GO TO 0030-RETURN.                                           PAR21A
016700 0020-SET-GOOD-RETURN.                                            PAR21A
016800     MOVE +0 TO RETURN-CODE.                                      PAR21A
017000 0030-RETURN.                                                     PAR21A
040300     EXIT PROGRAM.                                                PAR21A
040400/                                                                 PAR21A
017300******************************************************************PAR21A
040600*    THIS SECTION LOADS THE TITLE CODE PROFILE TABLE AND THE     *PAR21A
040700*    EXCLUDED TITLE CODE TABLE.  IN ADDITION, THE INPUT FILES    *PAR21A
040800*    FOR THE OTHER TABLES ARE OPENED AND PRIMED.                 *PAR21A
017500******************************************************************PAR21A
017700 1000-INITIALIZATION SECTION.                                     PAR21A
041100 1000-INIT-PARA.                                                  PAR21A
041200     OPEN INPUT                                                   PAR21A
041400                NINE-MONTH-CDS.                                   PAR21A
044500     PERFORM 3800-READ-NINE-MONTH.                                PAR21A
044600     IF NINE-MONTH-EOF                                            PAR21A
044700         DISPLAY MOD-NAME-C ' NINE MONTH FACULTY TITLE CD FILE '  PAR21A
044800                 'IS EMPTY  CANNOT PROCESS'                       PAR21A
044900         DISPLAY '         RUN TERMINATED'                        PAR21A
045000*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
045100         PERFORM 0010-SET-BAD-RETURN                              PAR21A
045200         PERFORM 0030-RETURN.                                     PAR21A
045300     MOVE +0 TO NUM-ENTRS-NM.                                     PAR21A
045400     MOVE ZERO TO MAX-CODE-NM.                                    PAR21A
045500     MOVE 9999 TO MIN-CODE-NM.                                    PAR21A
045600     MOVE '0' TO PROCESS-COMPLETE-SW.                             PAR21A
045700     PERFORM 3700-LOAD-NINE-MONTH                                 PAR21A
045800         UNTIL PROCESS-COMPLETE.                                  PAR21A
046100     CLOSE NINE-MONTH-CDS.                                        PAR21A
046200     OPEN INPUT EXCLUDED-FED-FUND.                                PAR21A
046500     PERFORM 5500-READ-EXCL-FED-FUND.                             PAR21A
020200     IF EXCLD-FED-FUND-EOF                                        PAR21A
020300         DISPLAY MOD-NAME-C ' EXCLUDED FEDERAL FUND TABLE FILE '  PAR21A
020400                 'IS EMPTY  NO EXCLUDED FUNDS TAKEN AS VALID'.    PAR21A
021400 1001-EXIT-INITIALIZATION.                                        PAR21A
021500     EXIT.                                                        PAR21A
048700/                                                                 PAR21A
061100 3700-LOAD-NINE-MONTH SECTION.                                    PAR21A
061200 3700-LOAD-9MO-PARA.                                              PAR21A
061300     IF NINE-MONTH-EOF                                            PAR21A
061400         SET NIN-IX3 TO NUM-ENTRS-NM                              PAR21A
061500         SET NIN-IX3 UP BY +1                                     PAR21A
061600         PERFORM 3900-PAD-NINE-MONTH                              PAR21A
061700             VARYING NIN-IX2 FROM NIN-IX3 BY +1                   PAR21A
061800             UNTIL NIN-IX2 > MAX-ENTRS-NM                         PAR21A
022500         MOVE '1' TO PROCESS-COMPLETE-SW                          PAR21A
062000         GO TO 3701-EXIT-LOAD-NINE-MONTH.                         PAR21A
062100     IF TITLE-CODE-NIN NUMERIC                                    PAR21A
024100         NEXT SENTENCE                                            PAR21A
024200     ELSE                                                         PAR21A
062400         DISPLAY MOD-NAME-C ' NINE MONTH TITLE CODE NOT NUMERIC ' PAR21A
062500                 'VALUE IS ' TITLE-CODE-NIN                       PAR21A
062600*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
062700         PERFORM 0010-SET-BAD-RETURN                              PAR21A
062800         PERFORM 0030-RETURN.                                     PAR21A
062900     IF TITLE-CODE-NIN > LAST-NINE-MONTH-CODE-WK                  PAR21A
063000         MOVE TITLE-CODE-NIN TO LAST-NINE-MONTH-CODE-WK           PAR21A
025300     ELSE                                                         PAR21A
063200         DISPLAY MOD-NAME-C ' NINE MONTH TITLE CODES OUT OF '     PAR21A
063300                 'SEQUENCE THIS CODE ' TITLE-CODE-NIN             PAR21A
063400                 ' LAST CODE ' LAST-NINE-MONTH-CODE-WK            PAR21A
063500*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
063600         PERFORM 0010-SET-BAD-RETURN                              PAR21A
063700         PERFORM 0030-RETURN.                                     PAR21A
063800     ADD +1 TO NUM-ENTRS-NM.                                      PAR21A
063900     IF NUM-ENTRS-NM > MAX-ENTRS-NM                               PAR21A
064000         DISPLAY MOD-NAME-C ' NUMBER ENTRIES IN NINE MONTH '      PAR21A
064100                 'FACULTY TABLE EXCEEDED MAX ENTRIES = '          PAR21A
064200                 MAX-ENTRS-NM                                     PAR21A
064300*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
064400         PERFORM 0010-SET-BAD-RETURN                              PAR21A
064500         PERFORM 0030-RETURN.                                     PAR21A
064600     SET NIN-IX1 TO NUM-ENTRS-NM.                                 PAR21A
064700     MOVE TITLE-CODE-NIN TO NINE-MON-CD-NM (NIN-IX1).             PAR21A
064800     IF TITLE-CODE-NIN > MAX-CODE-NM                              PAR21A
064900         MOVE TITLE-CODE-NIN TO MAX-CODE-NM.                      PAR21A
065000     IF TITLE-CODE-NIN < MIN-CODE-NM                              PAR21A
065100         MOVE TITLE-CODE-NIN TO MIN-CODE-NM.                      PAR21A
065200     PERFORM 3800-READ-NINE-MONTH.                                PAR21A
065300 3701-EXIT-LOAD-NINE-MONTH.                                       PAR21A
027400     EXIT.                                                        PAR21A
065500 3800-READ-NINE-MONTH SECTION.                                    PAR21A
065600 3800-READ-9MO-PARA.                                              PAR21A
065700     READ NINE-MONTH-CDS                                          PAR21A
027900         AT END                                                   PAR21A
065900             MOVE '1' TO NINE-MONTH-EOF-SW                        PAR21A
066000             GO TO 3801-EXIT-READ-NINE-MONTH.                     PAR21A
066100     ADD +1 TO NUM-NINE-MO-WK.                                    PAR21A
066200 3801-EXIT-READ-NINE-MONTH.                                       PAR21A
028400     EXIT.                                                        PAR21A
066400 3900-PAD-NINE-MONTH SECTION.                                     PAR21A
066500 3900-PAD-9MO-PARA.                                               PAR21A
066600     MOVE 9999 TO NINE-MON-CD-NM (NIN-IX2).                       PAR21A
066700 3901-EXIT-PAD-NINE-MONTH.                                        PAR21A
066800     EXIT.                                                        PAR21A
066900/                                                                 PAR21A
029200******************************************************************PAR21A
029300*    THIS SECTION CONTROLS THE LOADING OF THE EXCLUDED FEDERAL   *PAR21A
067200*    FUND TABLE, THE EXCLUDED DEPARTMENT TABLE, AND THE          *PAR21A
029500*    PRINCIPAL INVESTIGATOR TABLE.  THE VALUES IN THESE TABLES   *PAR21A
029600*    ARE DIFFERENT FOR EACH CAMPUS.                              *PAR21A
029700******************************************************************PAR21A
029900 4000-PROCESS SECTION.                                            PAR21A
067700 4000-PROC-PARA.                                                  PAR21A
030100     MOVE '0' TO PROCESS-COMPLETE-SW.                             PAR21A
030200     MOVE +0 TO NUM-ENTRIES-EFF.                                  PAR21A
030400     PERFORM 5000-LOAD-EXCL-FED-FUND                              PAR21A
030500         UNTIL PROCESS-COMPLETE.                                  PAR21A
031600 4001-EXIT-PROCESS.                                               PAR21A
031700     EXIT.                                                        PAR21A
069500/                                                                 PAR21A
031900 5000-LOAD-EXCL-FED-FUND SECTION.                                 PAR21A
069700 5000-LOAD-EX-FEDFUND-PARA.                                       PAR21A
032100     IF CAMPUS-CODE-EFR > LOCATION-CODE-LS OR                     PAR21A
032200        EXCLD-FED-FUND-EOF                                        PAR21A
032300         SET EFF-IX3 TO NUM-ENTRIES-EFF                           PAR21A
032400         SET EFF-IX3 UP BY +1                                     PAR21A
032500         PERFORM 5600-PAD-EXCL-FED-FUND                           PAR21A
032600             VARYING EFF-IX2 FROM EFF-IX3 BY +1                   PAR21A
032700             UNTIL EFF-IX2 > MAX-NUM-ENTRIES-EFF                  PAR21A
032800         MOVE '1' TO PROCESS-COMPLETE-SW                          PAR21A
032900         GO TO 5001-EXIT-LOAD-EXCL-FED-FUND.                      PAR21A
033100     IF CAMPUS-CODE-EFR < LOCATION-CODE-LS                        PAR21A
033200         PERFORM 5500-READ-EXCL-FED-FUND                          PAR21A
033300         GO TO 5001-EXIT-LOAD-EXCL-FED-FUND.                      PAR21A
033500     IF FUND-LOW-EFR NUMERIC AND                                  PAR21A
033600        FUND-HIGH-EFR NUMERIC                                     PAR21A
033700         NEXT SENTENCE                                            PAR21A
033800     ELSE                                                         PAR21A
033900         DISPLAY MOD-NAME-C ' EXCLUDED FEDERAL FUND RANGE NOT '   PAR21A
034000                 'NUMERIC  LOW VALUE = ' FUND-LOW-EFR             PAR21A
034100                 ' HIGH VALUE = ' FUND-HIGH-EFR                   PAR21A
071700*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
071800         PERFORM 0010-SET-BAD-RETURN                              PAR21A
071900         PERFORM 0030-RETURN.                                     PAR21A
034400     IF FUND-HIGH-EFR < FUND-LOW-EFR                              PAR21A
034500         DISPLAY MOD-NAME-C ' EXCLUDED FEDERAL FUND RANGE LOW '   PAR21A
034600                 'VALUE GREATER THAN HIGH VALUE. '                PAR21A
034700                 ' LOW VALUE = ' FUND-LOW-EFR                     PAR21A
034800                 ' HIGH VALUE = ' FUND-HIGH-EFR                   PAR21A
072500*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
072600         PERFORM 0010-SET-BAD-RETURN                              PAR21A
072700         PERFORM 0030-RETURN.                                     PAR21A
035100     ADD +1 TO NUM-ENTRIES-EFF.                                   PAR21A
035200     IF NUM-ENTRIES-EFF > MAX-NUM-ENTRIES-EFF                     PAR21A
035300         DISPLAY MOD-NAME-C ' EXCLUDED FEDERAL FUND TABLE SIZE '  PAR21A
035400                 'EXCEEDED.  MAX ENTRIES ' MAX-NUM-ENTRIES-EFF    PAR21A
073200*        GO TO 0010-SET-BAD-RETURN.                               PAR21A
073300         PERFORM 0010-SET-BAD-RETURN                              PAR21A
073400         PERFORM 0030-RETURN.                                     PAR21A
035700     SET EFF-IX1 TO NUM-ENTRIES-EFF.                              PAR21A
035800     MOVE FUND-LOW-EFR TO EXC-FUND-LOW-EFF (EFF-IX1).             PAR21A
035900     MOVE FUND-HIGH-EFR TO EXC-FUND-HIGH-EFF (EFF-IX1).           PAR21A
036100     PERFORM 5500-READ-EXCL-FED-FUND.                             PAR21A
036300 5001-EXIT-LOAD-EXCL-FED-FUND.                                    PAR21A
036400     EXIT.                                                        PAR21A
036600 5500-READ-EXCL-FED-FUND SECTION.                                 PAR21A
074200 5500-READ-EX-FEDFUND-PARA.                                       PAR21A
036800     READ EXCLUDED-FED-FUND                                       PAR21A
036900         AT END                                                   PAR21A
037000             MOVE '1' TO EXCLD-FED-FUND-EOF-SW                    PAR21A
037100             GO TO 5501-EXIT-READ-EXCL-FED-FUND.                  PAR21A
037300     ADD +1 TO NUM-EXCL-FED-FUND-WK.                              PAR21A
037500 5501-EXIT-READ-EXCL-FED-FUND.                                    PAR21A
037600     EXIT.                                                        PAR21A
037800 5600-PAD-EXCL-FED-FUND SECTION.                                  PAR21A
075100 5600-PAD-EX-FED-PARA.                                            PAR21A
038000     MOVE 99999 TO EXC-FUND-LOW-EFF (EFF-IX2)                     PAR21A
038100                   EXC-FUND-HIGH-EFF (EFF-IX2).                   PAR21A
038300 5601-EXIT-PAD-EXCL-FED-FUND.                                     PAR21A
038400     EXIT.                                                        PAR21A
075600/                                                                 PAR21A
097100/                                                                 PAR21A
047200 9000-TERMINATION SECTION.                                        PAR21A
097300 9000-TERM-PARA.                                                  PAR21A
097400     CLOSE EXCLUDED-FED-FUND.                                     PAR21A
047600     DISPLAY MOD-NAME-C ' END OF PROCESSING'.                     PAR21A
098200     DISPLAY '         NUMBER NINE MONTH FACULTY CODES = '        PAR21A
098300             NUM-NINE-MO-WK.                                      PAR21A
047900     DISPLAY '         NUMBER EXCLUDED FEDERAL FUND RCDS = '      PAR21A
048000             NUM-EXCL-FED-FUND-WK.                                PAR21A
048300 9001-EXIT-TERMINATION.                                           PAR21A
048400     EXIT.                                                        PAR21A
