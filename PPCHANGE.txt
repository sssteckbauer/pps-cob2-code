000100**************************************************************/   36070559
000200*  PROGRAM: PPCHANGE                                         */   36070559
000300*  RELEASE: ____0559____  SERVICE REQUEST(S): ____3607____   */   36070559
000400*  REFERENCE REL: _0535_                                     */   36070559
000500*  NAME:____JLT___________CREATION DATE:      __04/18/91__   */   36070559
000600*  DESCRIPTION:                                              */   36070559
000700*  - CORRECTED TO PLACE ACTIONS "01" AND "02" ON AUDIT FILE. */   36070559
000800**************************************************************/   36070559
000100**************************************************************/   30930413
000200*  PROGRAM: PPCHANGE                                         */   30930413
000300*  RELEASE: ____0413____  SERVICE REQUEST(S): ____3093____   */   30930413
000400*  NAME:____JIM WILLIAMS__CREATION DATE:      __05/23/89__   */   30930413
000500*  DESCRIPTION:                                              */   30930413
000600*  - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II. */   30930413
000700**************************************************************/   30930413
000800**************************************************************/   30890361
000900*  PROGRAM:  PPCHANGE                                        */   30890361
001000*  REL#:  0361   REF: NONE  SERVICE REQUESTS:   ___3089_____ */   30890361
001100*  NAME _____WEJ_______   MODIFICATION DATE ____07/01/88_____*/   30890361
001200*  DESCRIPTION                                               */   30890361
001300*   - CONVERT 'ENTRY' POINTS TO PARAGRAPHS TO ALLOW          */   30890361
001400*     DYNAMIC LINKING.                                       */   30890361
001500*                                                            */   30890361
001600**************************************************************/   30890361
001700 IDENTIFICATION DIVISION.                                         08/06/79
001800 PROGRAM-ID. PPCHANGE                                             PPCHANGE
001900 AUTHOR. INTEGRAL SYSTEMS, INC.                                      LV002
002000 INSTALLATION. INTEGRAL SYSTEMS, INC. STANDARD SYSTEM             PPCHANGE
002100     SKIP1                                                        PPCHANGE
002200******************************************************************PPCHANGE
002300*    THIS SUBPROGRAM CREATES THE AUDIT TRAIL, OR CHANGE LIST, FOR*PPCHANGE
002400*    FILE MAINTENANCE. IT IS CALLED FROM PP311200 AND PRODUCES 3*PPPCHANGE
002500*    TYPES OF RECORDS :                                          *PPCHANGE
002600*                                                                *PPCHANGE
002700*    1. A BEFORE IMAGE OF THE SEGMENT IS CREATED AT ENTRY POINT  *PPCHANGE
002800*       'BEFORIMG'. RECORD TYPE IS ZERO AND EFF. DATE  IS ZERO.  *PPCHANGE
002900*                                                                *PPCHANGE
003000*    2. AN AFTER IMAGE OF THE SEGMENT IS PRODUCED AT ENTRY POINT *PPCHANGE
003100*       'AUDIT'. RECORD TYPE IS ZERO AND EFF. DATE IS NOT ZERO.  *PPCHANGE
003200*                                                                *PPCHANGE
003300*    3. A CHANGE RECORD IS CREATED AT ENTRY POINT 'AUDIT'. THIS I*PPCHANGE
003400*        A LIST OF FIELD NUMBERS THAT HAVE BEEN CHANGED.         *PPCHANGE
003500*                                                                *PPCHANGE
003600*    ALL I/O TO THE CHANGE FILE IS DONE THRU THIS SUBPROGRAM.    *PPCHANGE
003700*                                                                *PPCHANGE
003800*    ENTRY POINTS: BEFORIMG AND AUDIT                            *PPCHANGE
003900*                                                                *PPCHANGE
004000******************************************************************PPCHANGE
004100     EJECT                                                        PPCHANGE
004200 DATE-WRITTEN.  DEC. 21, 1978. VERSION 4.0.                       PPCHANGE
004300 DATE-COMPILED.                                                   PPCHANGE
004400     SKIP1                                                        PPCHANGE
004500 ENVIRONMENT DIVISION.                                            PPCHANGE
004600 CONFIGURATION SECTION.                                           PPCHANGE
004700 SOURCE-COMPUTER. COPY CPOTXISI.                                  PPCHANGE
004800 OBJECT-COMPUTER. COPY CPOTXOBJ.                                  PPCHANGE
004900     SKIP2                                                        PPCHANGE
005000 INPUT-OUTPUT SECTION.                                            PPCHANGE
005100 FILE-CONTROL.                                                    PPCHANGE
005200     SKIP1                                                        PPCHANGE
005300     SELECT CHANGE-FILE  COPY  CPSLXECF.                          PPCHANGE
005400     EJECT                                                        PPCHANGE
005500 DATA DIVISION.                                                   PPCHANGE
005600 FILE SECTION.                                                    PPCHANGE
005700 FD  CHANGE-FILE    COPY  CPFDXECF.                               PPCHANGE
005800*01  EMPLOYEE-CHANGE-AREA  COPY  CPWSXECF.                        30930413
005900 01  EMPLOYEE-CHANGE-AREA. COPY  CPWSXECF.                        30930413
006000     SKIP1                                                        PPCHANGE
006100******************************************************************PPCHANGE
006200*    ACT-PERS-ACT-SET ARE THE PERSONNEL ACTIONS FOR THIS         *PPCHANGE
006300*    EMPLYEE/EFF. DATE. ACT-FIELD-SET IS THE TABLE OF FIELD      *PPCHANGE
006400*    NUMBERS WHICH HAVE BEEN CHANGED. ACT-FIELD-ACT              *PPCHANGE
006500*    IS ALWAYS SET TO 2 TO INDICATE CHANGE. ACT-FIELD-ERR        *PPCHANGE
006600*    IS TAKEN FROM THE DATA-LIST TABLE. RECORD TYPE IS 0 FOR     *PPCHANGE
006700*    SEGMENT IMAGES AND 1 FOR CHANGE LIST.                       *PPCHANGE
006800******************************************************************PPCHANGE
006900     EJECT                                                        PPCHANGE
007000 WORKING-STORAGE SECTION.                                         PPCHANGE
007100 77  SEG-INDEX                   PICTURE S999 COMP SYNC VALUE +0. PPCHANGE
007200 77  COMM-INDEX                  PICTURE S999 COMP SYNC VALUE +0. PPCHANGE
007300 77  PERS-INDEX                  PICTURE S999 COMP SYNC VALUE +0. PPCHANGE
007400 77  HOLD-POINT                  PICTURE S999 COMP SYNC VALUE +0. PPCHANGE
007500 77  F-OPEN                      PICTURE 9 VALUE 0.               PPCHANGE
007600     88 FILE-OPEN                VALUE 1.                         PPCHANGE
007700 77  CHG-INDEX                   PICTURE S999 COMP SYNC VALUE +0. PPCHANGE
007800     SKIP2                                                        PPCHANGE
007900 01  ACTION-HOLD.                                                 PPCHANGE
008000     03 HOLD-ACTION              PICTURE 99 OCCURS 5.             PPCHANGE
008100     SKIP2                                                        PPCHANGE
008200*01  INSTALLATION-CONSTANTS COPY CPWSXIDC.                        30930413
008300 01  INSTALLATION-CONSTANTS.    COPY CPWSXIDC.                    30930413
008400     SKIP1                                                        PPCHANGE
008500******************************************************************PPCHANGE
008600*    MAX-PERS-ACTIONS IS LENGTH OF PERSONNEL-ACTION TABLE IN     *PPCHANGE
008700*    COMM-TAB. MAX-ENTRIES-PER-SEG IS THE NUMBER OF FIELD        *PPCHANGE
008800*    ENTRIES THAT WILL FIT INTO THE CHANGE LIST RECORD. THIS IS  *PPCHANGE
008900*    A FUNCTION OF THE SEGMENT SIZE.                             *PPCHANGE
009000******************************************************************PPCHANGE
009100     EJECT                                                        PPCHANGE
009200 LINKAGE SECTION.                                                 PPCHANGE
009300*01  DATA-LIST COPY CPWSXDLT.                                     30930413
009400 01  DATA-LIST.    COPY CPWSXDLT.                                 30930413
009500     SKIP2                                                        PPCHANGE
009600*01  COMM-TAB COPY 'CPOTXCMF'.                                    30930413
009700 01  COMM-TAB.    COPY 'CPOTXCMF'.                                30930413
009800     SKIP2                                                        PPCHANGE
009900*01  SEGMENT-AREA COPY CPWSXEST.                                  30930413
010000 01  SEGMENT-AREA.    COPY CPWSXEST.                              30930413
010100     SKIP1                                                        PPCHANGE
010200******************************************************************PPCHANGE
010300*    THIS DESCRIBES THE SEGMENT AREA WHICH IS PASSED TO THIS     *PPCHANGE
010400*    SUBPROGRAM. THE OCCURS    DOES NOT REALLY TAKE UP ANY       *PPCHANGE
010500*    CORE SINCE THIS IS ONLY A PARAMETER AND CORE IS ACTUALLY    *PPCHANGE
010600*    ALLOCATED IN THE CALLING PROGRAM TO WHATEVER IS NECCESSARY. *PPCHANGE
010700******************************************************************PPCHANGE
010800     SKIP1                                                        30890361
010900*01  FUNCTION-UTILITY  COPY 'CPWSXFUT'.                           30930413
011000 01  FUNCTION-UTILITY.     COPY 'CPWSXFUT'.                       30930413
011100     EJECT                                                        PPCHANGE
011200*PROCEDURE DIVISION.                                              30890361
011300 PROCEDURE DIVISION  USING  DATA-LIST                             30890361
011400                            COMM-TAB                              30890361
011500                            SEGMENT-AREA                          30890361
011600                            FUNCTION-UTILITY.                     30890361
011700     SKIP1                                                        30890361
011800     IF BEFORE-IMAGE-CALL                                         30890361
011900        GO TO BEFORIMG-1000                                       30890361
012000     ELSE                                                         30890361
012100     IF AUDIT-PROCESS-CALL                                        30890361
012200        GO TO AUDIT-1010                                          30890361
012300     ELSE                                                         30890361
012400         MOVE '*INVALID' TO FUNCTION-UTILITY                      30890361
012500         GOBACK.                                                  30890361
012600     SKIP2                                                        30890361
012700 BEFORIMG-1000.                                                   30890361
012800*****ENTRY 'BEFORIMG' USING SEGMENT-AREA.                         30890361
012900     SKIP1                                                        PPCHANGE
013000     IF NOT FILE-OPEN                                             PPCHANGE
013100         OPEN OUTPUT CHANGE-FILE                                  PPCHANGE
013200         MOVE 1 TO F-OPEN                                         PPCHANGE
013300     ELSE                                                         PPCHANGE
013400         IF SEG-SS-NUM (1) = ALL '9'                              PPCHANGE
013500         CLOSE CHANGE-FILE                                        PPCHANGE
013600     ELSE                                                         PPCHANGE
013700         MOVE SEG-MENT (1) TO EMP-CHANGE-RECORD                   PPCHANGE
013800         MOVE ZEROS TO EMPCH-ID-FIELDS                            PPCHANGE
013900         WRITE EMPLOYEE-CHANGE-AREA.                              PPCHANGE
014000     GO TO IMG-END-1050.                                          PPCHANGE
014100     SKIP1                                                        PPCHANGE
014200******************************************************************PPCHANGE
014300*    THIS ENTRY POINT WRITES OUT THE BEFORE IMAGE. SEGMENT-AREA  *PPCHANGE
014400*    IS THE PARAMETER BUT WE ARE REALLY ONLY PICKING UP 1 SEGMENT*PPCHANGE
014500*    SEG-MENT (1) IS THE ONLY SEGMENT REFERENCED HERE.           *PPCHANGE
014600*                                                                *PPCHANGE
014700*    THE FIRST CALL TO THIS ENTRY POINT WILL OPEN THE FILE BUT   *PPCHANGE
014800*    NOT WRITE ANYTHING TO THE FILE (PP311200 MAKES THE INITIAL   PPCHANGE
014900*    CALL DURING HOUSEKEEPING THERE). IF AN SS NUMBER OF ALL     *PPCHANGE
015000*    9'S IS PASSED,      THE FILE IS CLOSED AND NO RECORD IS     *PPCHANGE
015100*    WRITTEN. WHENEVER A RECORD IS WRITTEN, RECORD TYPE, EFF.    *PPCHANGE
015200*    DATE, BATCH NO., AND ORIGIN ARE ALL SET TO ZERO.            *PPCHANGE
015300******************************************************************PPCHANGE
015400     EJECT                                                        PPCHANGE
015500 AUDIT-1010.                                                      30890361
015600*****ENTRY 'AUDIT' USING SEGMENT-AREA COMM-TAB DATA-LIST.         30890361
015700     SKIP1                                                        PPCHANGE
015800******************************************************************PPCHANGE
015900*    THIS ENTRY POINT IS CALLED AT THE END OF PROCESSING FOR     *PPCHANGE
016000*    AN SS/EFF. DATE AND PRODUCES THE AFTER IMAGE AND CHANGE     *PPCHANGE
016100*    LIST. PARAMETERS ARE AS FOLLOWS:                            *PPCHANGE
016200*    1. SEGMENT-AREA - THE UPDATED SEGMENTS                      *PPCHANGE
016300*    2. COMM-TAB - TO DETERMINE WHICH SEGMENTS HAVE BEEN UPDATED.*PPCHANGE
016400*    3. DATA-LIST - TO GET LIST OF FIELD NUMBERS.                *PPCHANGE
016500*                                                                *PPCHANGE
016600*    IF A SEGMENT HAS BEEN UPDATED, THIS ROUTINE FIRST CONSTRUCTS*PPCHANGE
016700*    THE SEGMENT IMAGE RECORD AND WRITES IT,      CHAINS THRU    *PPCHANGE
016800*    DATA-LIST TO BUILD THE CHANGE-LIST,      WRITES THE CHANGE  *PPCHANGE
016900*    LIST.                                                       *PPCHANGE
017000******************************************************************PPCHANGE
017100     SKIP1                                                        PPCHANGE
017200     MOVE ZERO TO ACTION-HOLD, COMM-INDEX, PERS-INDEX.            PPCHANGE
017300     PERFORM PERS-ACTION-2000.                                    PPCHANGE
017400     MOVE ZERO TO SEG-INDEX.                                      PPCHANGE
017500 AFTER-IMG-LOOP-1020.                                             PPCHANGE
017600     SKIP1                                                        PPCHANGE
017700******************************************************************PPCHANGE
017800*    THIS PARA. LOOPS THRU THE SEGMENTS IN SEGMENT-AREA TO       *PPCHANGE
017900*    DETERMINE IF THERE WERE ANY UPDATES. IT DOES THIS BY        *PPCHANGE
018000*    CHECKING WHETHER THERE IS ANYTHING IN DATA-LIST FOR         *PPCHANGE
018100*    THAT SEGMENT TYPE.  IF, THRU FINDING DATA-LIST ENTRIES, WE  *PPCHANGE
018200*    DETERMINE THAT A SEGMENT WAS CHANGED      WE CONSTRUCT THE  *PPCHANGE
018300*    AFTER IMAGE RECORD AND WRITE IT OUT.ONCE WE WRITE OUT       *PPCHANGE
018400*    THE SEGMENT IMAGE WE MUST WRITE OUT A CHANGE LIST RECORD.   *PPCHANGE
018500*    TO DO THAT WE INITIALIZE THE CHANGE LIST RECORD AND         *PPCHANGE
018600*    FALL THRU TO THE NEXT PARA   WHERE THE CHANGE LIST IS       *PPCHANGE
018700*    CONSTRUCTED.                                                *PPCHANGE
018800******************************************************************PPCHANGE
018900     SKIP1                                                        PPCHANGE
019000     ADD 1 TO SEG-INDEX.                                          PPCHANGE
019100     IF SEG-INDEX > DL-SEGMENTS                                   PPCHANGE
019200         GO TO IMG-END-1050.                                      PPCHANGE
019300     MOVE ZERO TO CHG-INDEX.                                      PPCHANGE
019400     IF POINT-FIRST (SEG-INDEX) = ZERO                            PPCHANGE
019500         AND SEGMENT-STATUS-TABLE (SEG-INDEX) < 2                 PPCHANGE
019600         GO TO AFTER-IMG-LOOP-1020.                               PPCHANGE
019700     SKIP1                                                        PPCHANGE
019800     MOVE SEG-MENT (SEG-INDEX) TO EMP-CHANGE-RECORD.              PPCHANGE
019900     MOVE ZERO TO EMPCH-RECORD-TYPE.                              PPCHANGE
020000     MOVE CURRENT-EFF-DATE TO EMPCH-EFFECT-DATE.                  PPCHANGE
020100     MOVE COMM-BATCH-NO TO EMPCH-BATCH-NO.                        PPCHANGE
020200     MOVE COMM-ORIGIN TO EMPCH-ORIGINATE.                         PPCHANGE
020300     WRITE EMPLOYEE-CHANGE-AREA.                                  PPCHANGE
020400     SKIP2                                                        PPCHANGE
020500 INITIAL-CHG-REC-1025.                                            PPCHANGE
020600     MOVE ZERO TO EMPCH-ACTIV-SET.                                PPCHANGE
020700     MOVE 01 TO EMPCH-RECORD-TYPE.                                PPCHANGE
020800     MOVE CURRENT-EFF-DATE TO EMPCH-EFFECT-DATE.                  PPCHANGE
020900     MOVE COMM-BATCH-NO TO EMPCH-BATCH-NO.                        PPCHANGE
021000     MOVE COMM-ORIGIN TO EMPCH-ORIGINATE.                         PPCHANGE
021100     MOVE CURRENT-SS-NUM TO EMPCH-SS-NO.                          PPCHANGE
021200     MOVE ZERO TO EMPCH-SEG-END.                                  PPCHANGE
021300     MOVE SEGMENT-IND (SEG-INDEX) TO EMPCH-SEG-STRT.              PPCHANGE
021400     IF CHG-INDEX = 0                                                CL**2
021500         MOVE POINT-FIRST (SEG-INDEX) TO HOLD-POINT.                 CL**2
021600     SKIP1                                                        PPCHANGE
021700 DATA-LIST-LOOP-1030.                                             PPCHANGE
021800     SKIP1                                                        PPCHANGE
021900******************************************************************PPCHANGE
022000*    THIS PARA. CHAINS THRU DATA-LIST PICKING UP FIELD           *PPCHANGE
022100*    NUMBERS FOR THIS SEGMENT. WHEN POINT-NEXT IS ZERO           *PPCHANGE
022200*    WE ARE AT THE END OF CHAIN.IF WE CAN'T FIT THEM ALL         *PPCHANGE
022300*    IN WE SET COMM-ERROR-CODE TO 1 AND QUIT FOR THAT            *PPCHANGE
022400*    SEGMENT.                                                    *PPCHANGE
022500******************************************************************PPCHANGE
022600     SKIP1                                                        PPCHANGE
022700     IF HOLD-POINT = ZERO                                         PPCHANGE
022800         GO TO WRITE-CHG-REC-1040.                                PPCHANGE
022900     ADD 1 TO CHG-INDEX.                                          PPCHANGE
023000     IF CHG-INDEX > MAX-ENTRIES-PER-SEG                           PPCHANGE
023100*        THE FOLLOWING THREE STATEMENTS ARE PROGRAM MODIFICATIONS PPCHANGE
023200*        TO ALLOW MORE THAN 50 ELEMENT CHANGES PER SEG.           PPCHANGE
023300         PERFORM WRITE-CHG-REC-1040                               PPCHANGE
023400         MOVE 1 TO CHG-INDEX                                      PPCHANGE
023500         PERFORM INITIAL-CHG-REC-1025.                            PPCHANGE
023600     MOVE DATA-FIELD (HOLD-POINT) TO EMPCH-FIELD-NO (CHG-INDEX).  PPCHANGE
023700     MOVE DATA-STATUS (HOLD-POINT) TO EMPCH-FIELD-ERR (CHG-INDEX).PPCHANGE
023800     MOVE 2 TO EMPCH-FIELD-ACT (CHG-INDEX).                       PPCHANGE
023900     MOVE POINT-NEXT (HOLD-POINT) TO HOLD-POINT.                  PPCHANGE
024000     GO TO DATA-LIST-LOOP-1030.                                   PPCHANGE
024100     SKIP1                                                        PPCHANGE
024200 WRITE-CHG-REC-1040.                                              PPCHANGE
024300     SKIP1                                                        PPCHANGE
024400******************************************************************PPCHANGE
024500*    HERE WE MOVE IN THE PERSONNEL ACTIONS, WRITE OUT            *PPCHANGE
024600*    THE CHANGE LIST RECORD AND GO BACK FOT THE NEXT SEGMENT.    *PPCHANGE
024700******************************************************************PPCHANGE
024800     SKIP1                                                        PPCHANGE
024900     MOVE ACTION-HOLD TO EMPCH-PERS-ACT-SET.                      PPCHANGE
025000     WRITE EMPLOYEE-CHANGE-AREA.                                  PPCHANGE
025100 CHECK-NEXT-SEG-1041.                                             PPCHANGE
025200     GO TO AFTER-IMG-LOOP-1020.                                   PPCHANGE
025300     SKIP1                                                        PPCHANGE
025400 IMG-END-1050.                                                    PPCHANGE
025500     GOBACK.                                                      PPCHANGE
025600     EJECT                                                        PPCHANGE
025700 PERS-ACTION-2000 SECTION.                                        PPCHANGE
025800     SKIP1                                                        PPCHANGE
025900******************************************************************PPCHANGE
026000*    THIS PARA. CONVERTS THE PERSONNEL ACTIONS FROM THE          *PPCHANGE
026100*    FORMAT IN WHICH THEY ARE STORED IN COMM-TAB TO THE          *PPCHANGE
026200*    FORMAT REQUIRED FOR CHANGE-LIST. E.G. IF PERSONNEL-         *PPCHANGE
026300*    ACTIONS IN COMM-TAB WAS '0199911999'      HOLD-ACTION       *PPCHANGE
026400*    WOULD BE SET TO '020607'.                                   *PPCHANGE
026500******************************************************************PPCHANGE
026600     SKIP1                                                        PPCHANGE
027500**** ADD 1 TO COMM-INDEX.                                         36070559
027600**** IF COMM-INDEX > MAX-PERS-ACTIONS                             36070559
027700****     GO TO PER-EXIT-2010.                                     36070559
027800**** IF PERSONNEL-ACTIONS (COMM-INDEX) = ZERO                     36070559
027900****     GO TO PERS-ACTION-2000.                                  36070559
028000**** ADD 1 TO PERS-INDEX.                                         36070559
028100**** MOVE COMM-INDEX TO HOLD-ACTION (PERS-INDEX).                 36070559
028200**** IF PERS-INDEX < MAX-ACTIONS-PER-SEG                          36070559
028300****     GO TO PERS-ACTION-2000.                                  36070559
028400*                                                                 36070559
028500     IF EMPL-ACTION-FLAG (1) = 'Y'                                36070559
028600         MOVE 01  TO HOLD-ACTION (1)                              36070559
028700     ELSE                                                         36070559
028800         IF EMPL-ACTION-FLAG (2) = 'Y'                            36070559
028900             MOVE 02  TO HOLD-ACTION (1)                          36070559
029000         END-IF                                                   36070559
029100     END-IF.                                                      36070559
027600 PER-EXIT-2010.                                                   PPCHANGE
027700     EXIT.                                                        PPCHANGE
027800     SKIP1                                                        PPCHANGE
027900******************************************************************PPCHANGE
028000*                                                                *PPCHANGE
028100*    E N D  S O U R C E ----- PPCHANGE -----                      PPCHANGE
028200*                                                                *PPCHANGE
028300*    12/27/73 MDZ                                                *PPCHANGE
028400*                                                                *PPCHANGE
028500******************************************************************PPCHANGE
028600*                                                                 PPCHANGE
