000100**************************************************************/   14420427
000200*  PROGRAM:  PPOT427                                         */   14420427
000300*  RELEASE # ____427___   SERVICE REQUEST NO(S) __1442______ */   14420427
000400*  NAME ___M. SANO_____                DATE ____11/01/89_____*/   14420427
000500*  DESCRIPTION                                               */   14420427
000600*      - ONE TIME PROGRAM TO ADD CBBC, BENEFIT TYPE AND      */   14420427
000700*        BENEFIT PLAN CODES TO THE GTN TABLE                 */   14420427
000800**************************************************************/   14420427
000900 IDENTIFICATION DIVISION.                                         PPOT427
001000 PROGRAM-ID. PPOT427                                              PPOT427
001100 AUTHOR. M. SANO                                                  PPOT427
001200 INSTALLATION. U.C.O.P                                            PPOT427
001300     SKIP1                                                        PPOT427
001400******************************************************************PPOT427
001500*    THIS PROGRAM PERFORMS SOME I/O FUNCTIONS ON THE             *PPOT427
001600*    PBP CONTROL FILE.                                           *PPOT427
001700*                                                                *PPOT427
001800*    FUNCTION-CODE IS THE REQUESTED OPERATION AND MUST BE        *PPOT427
001900*    ONE OF THE FOLLOWING:                                       *PPOT427
002000*        SEQ-OPEN-IO     SEQ-READ     SEQ-WRITE   SEQQ-CLOSE     *PPOT427
002100*                                                                *PPOT427
002200*    NOMINAL-KEY IS A 13 BYTE KEY AREA WHICH CONTAINS            *PPOT427
002300*    ZERO FOR SEQL READS.                                        *PPOT427
002400*                                                                *PPOT427
002500*    PBP-CONTROL-RECORD IS AN AREA WHERE THE SEGMENT IS*         *PPOT427
002600*    OR RETREIVED.THIS SHOULD BE A 224 BYTE AREA FOR             *PPOT427
002700*    DIRECT OPERATIONS OR SEQL OPERATIONS WHEN THE NOMINAL       *PPOT427
002800*    KEY IS ZERO.                                                *PPOT427
002900*                                                                *PPOT427
003000******************************************************************PPOT427
003100     SKIP1                                                        PPOT427
003200 DATE-WRITTEN.  SEPTEMBER 1, 1989.                                PPOT427
003300 DATE-COMPILED. SEPTEMBER 1, 1989.                                PPOT427
003400 ENVIRONMENT DIVISION.                                            PPOT427
003500 CONFIGURATION SECTION.                                           PPOT427
003600 SOURCE-COMPUTER. COPY CPOTXISI.                                  PPOT427
003700 OBJECT-COMPUTER. COPY CPOTXOBJ.                                  PPOT427
003800     SKIP2                                                        PPOT427
003900 INPUT-OUTPUT SECTION.                                            PPOT427
004000 FILE-CONTROL.                                                    PPOT427
004100     SKIP1                                                        PPOT427
004200     SELECT PBP-CONTROL-FILE         COPY CPSLXCTL.               PPOT427
004300                                                                  PPOT427
004400     SELECT CTL-RESTORE-FILE         COPY CPSLXRSC.               PPOT427
004500*I-O-CONTROL.                                                     PPOT427
004600*    APPLY CORE-INDEX ON PBP-CONTROL-FILE.                        PPOT427
004700*    SAME AREA PBP-CONTROL-FILE.                                  PPOT427
004800*    SAME RECORD AREA PBP-CONTROL-FILE.                           PPOT427
004900     EJECT                                                        PPOT427
005000 DATA DIVISION.                                                   PPOT427
005100 FILE SECTION.                                                    PPOT427
005200     SKIP2                                                        PPOT427
005300 FD  PBP-CONTROL-FILE                COPY CPFDXCTL.               PPOT427
005400                                                                  PPOT427
005500 FD  CTL-RESTORE-FILE                COPY CPFDXRSC.               PPOT427
005600      EJECT                                                       PPOT427
005700 WORKING-STORAGE SECTION.                                         PPOT427
005800     SKIP2                                                        PPOT427
005900 77  FUNC-INDEX                  PICTURE S99 COMP SYNC.           PPOT427
006000 77  LAST-SEQ-KEY                PICTURE X(13) VALUE SPACES.      PPOT427
006100     SKIP2                                                        PPOT427
006200 01  VSAM-STATUS.                                                 PPOT427
006300                                    COPY CPVSMSTA.                PPOT427
006400 01  VSAM-ABEND.                                                  PPOT427
006500                                    COPY CPVSMABE.                PPOT427
006600 01  FLAGS.                                                       PPOT427
006700     05 END-OF-FILE              PICTURE 9 VALUE 0.               PPOT427
006800        88 EOF                             VALUE 1.               PPOT427
006900     05 ERROR-FLAG               PICTURE 9 VALUE 0.               PPOT427
007000        88 ERROR-FOUND                     VALUE 1.               PPOT427
007100     05 GTN-WRITE-FLAG           PICTURE 9 VALUE 0.               PPOT427
007200        88 FIRST-GTN-WRITE                 VALUE 0.               PPOT427
007300     SKIP2                                                        PPOT427
007400*01  IO-FUNCTION.                                                 PPOT427
007500*                       COPY CPWSXIOF.                            PPOT427
007600     SKIP1                                                        PPOT427
007700 01  CTL-INTERFACE.                                               PPOT427
007800                    COPY CPWSXCIF.                                PPOT427
007900 01  CTL-SEGMENT-TABLE.                                           PPOT427
008000                        COPY CPWSXCST.                            PPOT427
008100 01  XGTN-GROSS-TO-NET-RECORD.                                    PPOT427
008200                        COPY CPWSXGTN.                            PPOT427
008300 01  WS-SEG-TAB-KEY.                                              PPOT427
008400     03  FILLER         PIC X(01).                                PPOT427
008500     03  WS-SEG-TAB-GTN PIC X(03).                                PPOT427
008600     03  FILLER         PIC X(09).                                PPOT427
008700 01  PAYROLL-PROCESS-INST-CONST-2.                                PPOT427
008800                        COPY CPWSXIC2.                            PPOT427
008900     EJECT                                                        PPOT427
009000******************************************************************PPOT427
009100*    THE FIRST TWO PARAGRAPHS VALIDATE THE FUNCTION CODE.        *PPOT427
009200*    FUNC-INDEX IS SET TO THE INDEX IN THE FUNCTION TABLE.       *PPOT427
009300******************************************************************PPOT427
009400 PROCEDURE DIVISION.                                              PPOT427
009500                                                                  PPOT427
009600 INITIALIZE-1000 SECTION.                                         PPOT427
009700     MOVE ZERO TO IO-CTL-ERROR-CODE, ERROR-FLAG.                  PPOT427
009800     PERFORM OPEN-IO-2030.                                        PPOT427
009900     OPEN OUTPUT CTL-RESTORE-FILE.                                PPOT427
010000     PERFORM CTL-PROCESSING-1100                                  PPOT427
010100               UNTIL EOF OR ERROR-FOUND.                          PPOT427
010200     PERFORM CLOSE-2070.                                          PPOT427
010300     CLOSE CTL-RESTORE-FILE.                                      PPOT427
010400     STOP RUN.                                                    PPOT427
010500                                                                  PPOT427
010600 CTL-PROCESSING-1100.                                             PPOT427
010700     PERFORM READ-2040.                                           PPOT427
010800     IF NOT (EOF OR ERROR-FOUND)                                  PPOT427
010900         PERFORM WRITE-2060.                                      PPOT427
011000                                                                  PPOT427
011100******************************************************************PPOT427
011200*    THIS PARAGRAPH PERFORMS OPEN OPERATION.                     *PPOT427
011300******************************************************************PPOT427
011400     SKIP1                                                        PPOT427
011500 OPEN-IO-2030.                                                    PPOT427
011600     OPEN I-O PBP-CONTROL-FILE.                                   PPOT427
011700     IF VSAM-STATUS NOT = '00'                                    PPOT427
011800         AND VSAM-STATUS NOT = '97'                               PPOT427
011900         MOVE '2030' TO VSAM-ABEND-PARAGRAPH                      PPOT427
012000         MOVE 'SEQ-OPEN-IO' TO VSAM-ATTEMPTED-OPERATION           PPOT427
012100         PERFORM VSAM-ABEND-2090.                                 PPOT427
012200                                                                  PPOT427
012300******************************************************************PPOT427
012400*    SEQL READ HAS THE FOLLOWING OPTION:                         *PPOT427
012500*       1. FUNCTION CODE IS <SEQ-READ>                           *PPOT427
012600*          THE CALLER SPECIFIES THE SEGMENT HE DESIRES IN        *PPOT427
012700*          IO-CTL-NOM-KEY, AND THIS IS THE ONLY SEGMENT RETURNED *PPOT427
012800*          IF IT IS PRESENT.                                     *PPOT427
012900******************************************************************PPOT427
013000 READ-2040.                                                       PPOT427
013100     MOVE 0 TO IO-CTL-NOM-KEY.                                    PPOT427
013200                                                                  PPOT427
013300     PERFORM CONTROL-READ-SEQL-2080.                              PPOT427
013400                                                                  PPOT427
013500     IF  NOT EOF                                                  PPOT427
013600         MOVE PBPCTL-CONTROL-RECORD TO CTL-SEG-TAB                PPOT427
013700         MOVE CTL-SEG-TAB-KEY TO WS-SEG-TAB-KEY.                  PPOT427
013800                                                                  PPOT427
013900******************************************************************PPOT427
014000*    SEQL WRITE ROUTINES FOLLOW. THE PARAGRAPH                   *PPOT427
014100*    DOES SEQUENCE CHECKING AND OPEN CHECKING BEFORE             *PPOT427
014200*    PERFORMING THE I/O OPERATION.                               *PPOT427
014300******************************************************************PPOT427
014400 WRITE-2060.                                                      PPOT427
014500     IF WS-SEG-TAB-GTN = 'GTN'                                    PPOT427
014600         IF FIRST-GTN-WRITE                                       PPOT427
014700             MOVE 1 TO GTN-WRITE-FLAG                             PPOT427
014800             WRITE CTL-RESTORE-RECORD FROM PBPCTL-CONTROL-RECORD  PPOT427
014900         ELSE                                                     PPOT427
015000             MOVE PBPCTL-CONTROL-RECORD TO                        PPOT427
015100                                 XGTN-GROSS-TO-NET-RECORD         PPOT427
015200             PERFORM INITIALIZE-3000                              PPOT427
015300             WRITE CTL-RESTORE-RECORD FROM                        PPOT427
015400                                 XGTN-GROSS-TO-NET-RECORD         PPOT427
015500     ELSE                                                         PPOT427
015600         WRITE CTL-RESTORE-RECORD FROM PBPCTL-CONTROL-RECORD.     PPOT427
015700                                                                  PPOT427
015800     IF VSAM-STATUS NOT = '00'                                    PPOT427
015900         MOVE '2060' TO VSAM-ABEND-PARAGRAPH                      PPOT427
016000         MOVE 'SEQ-WRITE' TO VSAM-ATTEMPTED-OPERATION             PPOT427
016100         PERFORM VSAM-ABEND-2090.                                 PPOT427
016200                                                                  PPOT427
016300******************************************************************PPOT427
016400*    SEQL CLOSETE ROUTINES FOLLOW. BOTH ROUTINES                 *PPOT427
016500*    DO SEQUENCE CHECKING AND OPEN CHECKING BEFORE               *PPOT427
016600*    PERFORMING THE I/O OPERATION.                               *PPOT427
016700******************************************************************PPOT427
016800 CLOSE-2070.                                                      PPOT427
016900     CLOSE PBP-CONTROL-FILE.                                      PPOT427
017000     IF VSAM-STATUS NOT = '00'                                    PPOT427
017100         MOVE '2070' TO VSAM-ATTEMPTED-OPERATION                  PPOT427
017200         MOVE 'SEQ-CLOSE' TO VSAM-ATTEMPTED-OPERATION             PPOT427
017300         PERFORM VSAM-ABEND-2090.                                 PPOT427
017400                                                                  PPOT427
017500******************************************************************PPOT427
017600*    END OF SEQUENTIAL PROCESSING                                *PPOT427
017700******************************************************************PPOT427
017800 CONTROL-READ-SEQL-2080.                                          PPOT427
017900     READ PBP-CONTROL-FILE.                                       PPOT427
018000     IF VSAM-STATUS-BYTE-1 = '1'                                  PPOT427
018100         MOVE 1 TO END-OF-FILE                                    PPOT427
018200         MOVE 6 TO IO-CTL-ERROR-CODE                              PPOT427
018300         MOVE ALL '9' TO PBPCTL-REC-KEY-AREA                      PPOT427
018400     ELSE                                                         PPOT427
018500         IF VSAM-STATUS NOT = '00'                                PPOT427
018600             MOVE '3010' TO VSAM-ABEND-PARAGRAPH                  PPOT427
018700             MOVE 'SEQ-READ' TO VSAM-ATTEMPTED-OPERATION          PPOT427
018800             PERFORM VSAM-ABEND-2090.                             PPOT427
018900     IF (PBPCTL-REC-DELETE = HIGH-VALUE) AND                      PPOT427
019000        (PBPCTL-REC-KEY-AREA NOT = ALL '9')                       PPOT427
019100        GO TO CONTROL-READ-SEQL-2080.                             PPOT427
019200     MOVE PBPCTL-REC-KEY-AREA TO LAST-SEQ-KEY.                    PPOT427
019300                                                                  PPOT427
019400******************************************************************PPOT427
019500*      THIS ROUTINE IS USED TO SET THE FILE STATUS FOR THE CALLER PPOT427
019600*      WHEN FOUND ON THE CALL PROGRAM WILL TELL CALLER HOW THE    PPOT427
019700*      FILE IS OPENED. THE CALLLER WILL      BE ABLE TO SET THE   PPOT427
019800*      PROPER READ MODEFOR ACCESSING THE CONTROL FILE             PPOT427
019900******************************************************************PPOT427
020000  VSAM-ABEND-2090.                                                PPOT427
020100      DISPLAY 'VSAM ABEND - PPIOCTL'                              PPOT427
020200          VSAM-ABEND-PARAGRAPH                                    PPOT427
020300          VSAM-ATTEMPTED-OPERATION                                PPOT427
020400          VSAM-STATUS                                             PPOT427
020500          FUNC-INDEX                                              PPOT427
020600          LAST-SEQ-KEY                                            PPOT427
020700          CTL-INTERFACE                                           PPOT427
020800          CTL-SEGMENT-TABLE.                                      PPOT427
020900      MOVE '24' TO IO-CTL-ERROR-CODE.                             PPOT427
021000      MOVE 1    TO ERROR-FLAG.                                    PPOT427
021100  INITIALIZE-3000.                                                PPOT427
021200      IF XGTN-GROUP = 'U'                                         PPOT427
021300         MOVE '1' TO XGTN-COLL-BARGN-ELIG-LEVEL-IND               PPOT427
021400         MOVE '2' TO XGTN-COLL-BARG-BEHAVIOR-CODE                 PPOT427
021500         MOVE SPACES  TO XGTN-BENEFIT-TYPE, XGTN-BENEFIT-PLAN     PPOT427
021600      ELSE                                                        PPOT427
021700         IF XGTN-GROUP = 'I'                                      PPOT427
021800            MOVE SPACES TO XGTN-COLL-BARGN-ELIG-LEVEL-IND,        PPOT427
021900                           XGTN-COLL-BARG-BEHAVIOR-CODE           PPOT427
022000            SET WT5-IX TO 1                                       PPOT427
022100            PERFORM INITIALIZE-INSURANCE-3050                     PPOT427
022200         ELSE                                                     PPOT427
022300            MOVE SPACES TO XGTN-COLL-BARGN-ELIG-LEVEL-IND,        PPOT427
022400                           XGTN-COLL-BARG-BEHAVIOR-CODE,          PPOT427
022500                           XGTN-BENEFIT-TYPE,                     PPOT427
022600                           XGTN-BENEFIT-PLAN.                     PPOT427
022700  INITIALIZE-INSURANCE-3050.                                      PPOT427
022800      SEARCH WT5-GTN-ENTRY VARYING WT5-IX                         PPOT427
022900             WHEN XGTN-DEDUCTION = WT5-GTN-NO (WT5-IX)            PPOT427
023000                  MOVE WT5-PLAN (WT5-IX) TO XGTN-BENEFIT-PLAN     PPOT427
023100                  MOVE WT5-INS-TYPE (WT5-IX) TO XGTN-BENEFIT-TYPE.PPOT427
023200      IF XGTN-DEDUCTION NOT = WT5-GTN-NO (WT5-IX)                 PPOT427
023300             MOVE SPACES TO XGTN-BENEFIT-PLAN                     PPOT427
023400                            XGTN-BENEFIT-TYPE.                    PPOT427
