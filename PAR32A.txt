000100 IDENTIFICATION DIVISION.                                         PAR32A
000200 PROGRAM-ID. PAR32A.                                              PAR32A
000300 DATE-WRITTEN. MAY 01, 1980.                                      PAR32A
000400 DATE-COMPILED.                                                   PAR32A
000500*REMARKS.                                                         PAR32A
000600******************************************************************PAR32A
000700*                                                                *PAR32A
000800* FUNCTION:                                                      *PAR32A
000900*    THIS PROGRAM PRODUCES THE EXTRACT EXCEPTION REPORT, PAR0302A*PAR32A
001000*    WHEN THE PAR EXTRACT IS RUN WITH SPECIAL TEST PARMS, THIS   *PAR32A
001100*    REPORT IS PRODUCED FOR ALL EMPLOYEES.  WHEN THE PAR EXTRACT *PAR32A
001200*    IS RUN IN PRODUCTION, ONLY PROCESSING EXCEPTIONS ARE        *PAR32A
001300*    PRINTED ON THIS REPORT.                                     *PAR32A
001400*                                                                *PAR32A
001500* INPUT FILES:                                                   *PAR32A
001600*    NONE                                                        *PAR32A
001700*                                                                *PAR32A
001800* OUTPUT FILES:                                                  *PAR32A
001900*    PARO03EX - EXTRACT EXCEPTION REPORT.                        *PAR32A
002000*                                                                *PAR32A
002100* PARM:                                                          *PAR32A
002200*    NONE                                                        *PAR32A
002300*                                                                *PAR32A
002400* CALLED MODULES:                                                *PAR32A
002500*    NONE                                                        *PAR32A
002600*                                                                *PAR32A
002700* CALLING MODULES:                                               *PAR32A
002800*    PAR0300A - EXTRACT REPORT CONTROL                           *PAR32A
002900*                                                                *PAR32A
003000* RETURN CODES:                                                  *PAR32A
003100*    0000 = REQUESTED FUCTION COMPLETED                          *PAR32A
003200*    0016 = ABNORMAL CONDITION ENCOUNTERED.  SEE DISPLAYS.       *PAR32A
003300*                                                                *PAR32A
003400* MODIFICATIONS:                                                 *PAR32A
003500*    DATE     WHO      MODIFICATION DESCRIPTION                  *PAR32A
003600*  XX/XX/XX   XXX   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *PAR32A
003700*                   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *PAR32A
003800*                                                                *PAR32A
003900******************************************************************PAR32A
004000 EJECT                                                            PAR32A
004100 ENVIRONMENT DIVISION.                                            PAR32A
004200 INPUT-OUTPUT SECTION.                                            PAR32A
004300 FILE-CONTROL.                                                    PAR32A
004400     SELECT EXTRACT-REPORT-OUT   ASSIGN TO UT-S-PARO03EX.         PAR32A
004500*    SELECT EXTRACT-REPORT-OUT   ASSIGN TO PRINTER.               PAR32A
004600 EJECT                                                            PAR32A
004700 DATA DIVISION.                                                   PAR32A
004800 FILE SECTION.                                                    PAR32A
004900 FD  EXTRACT-REPORT-OUT           RECORDING MODE IS F             PAR32A
005000                                  RECORD CONTAINS 133 CHARACTERS  PAR32A
005100                                  BLOCK CONTAINS 0 RECORDS        PAR32A
005200                                  LABEL RECORDS ARE STANDARD      PAR32A
005300                                  DATA RECORD IS                  PAR32A
005400                                     EXTRACT-PRINT-LINE.          PAR32A
005500*FD  EXTRACT-REPORT-OUT                                           PAR32A
005600*    LABEL RECORD IS OMITTED.                                     PAR32A
005700*                                                                 PAR32A
005800 01  EXTRACT-PRINT-LINE.                                          PAR32A
005900     05  CC-PL                    PIC 9.                          PAR32A
006000     05  FILLER                   PIC X(132).                     PAR32A
006100 EJECT                                                            PAR32A
006200 WORKING-STORAGE SECTION.                                         PAR32A
006300 01  HEADING-H1.                                                  PAR32A
006400*    05  FILLER                   PIC X VALUE '1'.                PAR32A
006500     05  FILLER                   PIC 9 VALUE 0.                  PAR32A
006600     05  FILLER                   PIC X(17)                       PAR32A
006700                                     VALUE ' REPORT  PAR0302R'.   PAR32A
006800     05  FILLER                   PIC X(27) VALUE SPACES.         PAR32A
006900     05  FILLER                   PIC X(35)                       PAR32A
007000                 VALUE 'PERSONNEL ACTIVITY REPORTING SYSTEM'.     PAR32A
007100     05  FILLER                   PIC X(30) VALUE SPACES.         PAR32A
007200     05  FILLER                   PIC X(6) VALUE 'PAGE  '.        PAR32A
007300     05  PAGE-NUM-H1              PIC ZZZZ9.                      PAR32A
007400     05  FILLER                   PIC X(12) VALUE SPACES.         PAR32A
007500 01  HEADING-H2.                                                  PAR32A
007600*    05  FILLER                   PIC X VALUE '0'.                PAR32A
007700     05  FILLER                   PIC 9 VALUE 2.                  PAR32A
007800     05  FILLER                   PIC X(49) VALUE SPACES.         PAR32A
007900     05  FILLER                   PIC X(24)                       PAR32A
008000                 VALUE 'EXTRACT EXCEPTION REPORT'.                PAR32A
008100     05  FILLER                   PIC X(36) VALUE SPACES.         PAR32A
008200     05  FILLER                   PIC X(6) VALUE 'DATE  '.        PAR32A
008300     05  MONTH-H2                 PIC 99.                         PAR32A
008400     05  FILLER                   PIC X VALUE '/'.                PAR32A
008500     05  DAY-H2                   PIC 99.                         PAR32A
008600     05  FILLER                   PIC X VALUE '/'.                PAR32A
008700     05  YEAR-H2                  PIC 99.                         PAR32A
008800     05  FILLER                   PIC X(09) VALUE SPACES.         PAR32A
008900 01  HEADING-H3.                                                  PAR32A
009000*    05  FILLER                   PIC X VALUE '0'.                PAR32A
009100     05  FILLER                   PIC 9 VALUE 2.                  PAR32A
009200     05  FILLER                   PIC X(12) VALUE '       HOME '. PAR32A
009300     05  FILLER                   PIC X(37)                       PAR32A
009400             VALUE '------------EMPLOYEE DATA------------'.       PAR32A
009500     05  FILLER                   PIC X(10) VALUE ' SEL TYP '.    PAR32A
009600     05  FILLER                   PIC X(39)                       PAR32A
009700             VALUE '-------------LEDGER ENTRIES------------'.     PAR32A
009800     05  FILLER                   PIC X(4) VALUE ' EXC'.          PAR32A
009900     05  FILLER                   PIC X(30) VALUE SPACES.         PAR32A
010000 01  HEADING-H4.                                                  PAR32A
010100*    05  FILLER                   PIC X VALUE ' '.                PAR32A
010200     05  FILLER                   PIC 9 VALUE 1.                  PAR32A
010300     05  FILLER                   PIC X(12) VALUE 'CAMPUS DEPT '. PAR32A
010400     05  FILLER                   PIC X(37)                       PAR32A
010500             VALUE '  NO      NAME                       '.       PAR32A
010600     05  FILLER                   PIC X(10) VALUE ' RSN PAR  '.   PAR32A
010700     05  FILLER                   PIC X(39)                       PAR32A
010800             VALUE 'TITL   ACCT  FUND  DATE TYP PCT DOLLARS'.     PAR32A
010900     05  FILLER                   PIC X(34)                       PAR32A
011000             VALUE ' CD   EXCEPTION DESCRIPTION       '.          PAR32A
011100 01  DETAIL-1.                                                    PAR32A
011200*    05  CC-D1                    PIC X.                          PAR32A
011300     05  CC-D1                    PIC 9 VALUE 1.                  PAR32A
011400     05  FILLER                   PIC X(2) VALUE SPACES.          PAR32A
011500     05  CAMPUS-D1                PIC 99.                         PAR32A
011600     05  FILLER                   PIC X(1) VALUE SPACES.          PAR32A
011700     05  HOME-DEPT-D1             PIC 999999.                     PAR32A
011800     05  FILLER                   PIC X VALUE SPACE.              PAR32A
011900     05  EMP-NO-D1                PIC 9(6).                       PAR32A
012000     05  FILLER                   PIC X VALUE SPACE.              PAR32A
012100     05  EMP-NAME-D1              PIC X(30).                      PAR32A
012200     05  FILLER                   PIC XX VALUE SPACES.            PAR32A
012300     05  SEL-RSN-D1               PIC XX.                         PAR32A
012400     05  FILLER                   PIC XX  VALUE SPACES.           PAR32A
012500     05  TYP-PAR-D1               PIC X.                          PAR32A
012600     05  FILLER                   PIC XXX VALUE SPACES.           PAR32A
012700     05  TITL-D1                  PIC 9(4).                       PAR32A
012800     05  FILLER                   PIC X VALUE SPACES.             PAR32A
012900     05  ACCT-D1                  PIC 9(6).                       PAR32A
013000     05  FILLER                   PIC X VALUE SPACES.             PAR32A
013100     05  FUND-D1                  PIC 9(5).                       PAR32A
013200     05  FILLER                   PIC XX VALUE SPACES.            PAR32A
013300     05  DATE-D1                  PIC 9(4).                       PAR32A
013400     05  FILLER                   PIC XX VALUE SPACES.            PAR32A
013500     05  ACCT-TYPE-D1             PIC X.                          PAR32A
013600     05  FILLER                   PIC XX VALUE SPACES.            PAR32A
013700     05  PCT-D1                   PIC ZZ9.                        PAR32A
013800     05  DOLLARS-D1               PIC ZZZZ9.99-.                  PAR32A
013900     05  FILLER                   PIC X VALUE SPACES.             PAR32A
014000     05  EXC-CD-D1                PIC 99.                         PAR32A
014100     05  FILLER                   PIC X VALUE SPACES.             PAR32A
014200     05  EXC-DESC-D1              PIC X(30).                      PAR32A
014300 01  DETAIL-2.                                                    PAR32A
014400     05  CC-D2                    PIC X VALUE ' '.                PAR32A
014500     05  CC-D2                    PIC 9 VALUE 1.                  PAR32A
014600     05  FILLER                   PIC XX VALUE SPACES.            PAR32A
014700     05  FILLER                   PIC X(22)                       PAR32A
014800                 VALUE 'DISTRIBUTION ACCOUNT  '.                  PAR32A
014900     05  DIST-ACCT-D2             PIC 9(6).                       PAR32A
015000     05  FILLER                   PIC X(102) VALUE SPACES.        PAR32A
015100 EJECT                                                            PAR32A
015200 01  EXCEPTION-TABLE-W.                                           PAR32A
015300     05  FILLER                   PIC X(30)                       PAR32A
015400                 VALUE 'ACCT # ENTRIES EXCEEDED       '.          PAR32A
015500     05  FILLER                   PIC X(30)                       PAR32A
015600                 VALUE 'EMPLOYEE NOT ON TITLE CODE TBL'.          PAR32A
015700     05  FILLER                   PIC X(30)                       PAR32A
015800                 VALUE 'EMPLOYEE NOT SELECTED         '.          PAR32A
015900     05  FILLER                   PIC X(30)                       PAR32A
016000                 VALUE 'NO PAR - NO EXTRACT DETAIL    '.          PAR32A
016100     05  FILLER                   PIC X(30)                       PAR32A
016200                 VALUE '                              '.          PAR32A
016300     05  FILLER                   PIC X(30)                       PAR32A
016400                 VALUE '                              '.          PAR32A
016500     05  FILLER                   PIC X(30)                       PAR32A
016600                 VALUE '                              '.          PAR32A
016700     05  FILLER                   PIC X(30)                       PAR32A
016800                 VALUE '                              '.          PAR32A
016900     05  FILLER                   PIC X(30)                       PAR32A
017000                 VALUE '                              '.          PAR32A
017100     05  FILLER                   PIC X(30)                       PAR32A
017200                 VALUE 'MAX NUMBER ERRORS EXCEEDED    '.          PAR32A
017300 01  EXCEPTION-TABLE-R-W          REDEFINES EXCEPTION-TABLE-W.    PAR32A
017400     05  EXC-DESC-W               OCCURS 10 TIMES                 PAR32A
017500                                  INDEXED BY EXC-IX               PAR32A
017600                                  PIC X(30).                      PAR32A
017700 EJECT                                                            PAR32A
017800 01  REPORT-WORK-AREA-W.                                          PAR32A
017900     05  MAX-LINES-PAGE-W         PIC S9(4) COMP SYNC VALUE +55.  PAR32A
018000     05  NUM-LINES-PAGE-W         PIC S9(4) COMP SYNC VALUE +0.   PAR32A
018100     05  PAGE-NUM-W               PIC S9(5) COMP-3 VALUE +0.      PAR32A
018200 01  WORK-AREAS-W.                                                PAR32A
018300     05  NUM-EMP-PRINTED-W        PIC S9(9) COMP-3 VALUE +0.      PAR32A
018400     05  NUM-EMP-ERROR-W          PIC S9(9) COMP-3 VALUE +0.      PAR32A
018500     05  MOD-NAME-C               PIC X(8) VALUE 'PAR0302B'.      PAR32A
018600     05  LAST-EMP-NUM-W           PIC 9(9) VALUE ZEROS.           PAR32A
018700 EJECT                                                            PAR32A
018800 LINKAGE SECTION.                                                 PAR32A
018900 01  MODULE-COMMON-AREA.          COPY 'PARYMODC'.                PAR32A
019000*                                                                 PAR32A
019100 EJECT                                                            PAR32A
019200*01  EMPLOYEE-DATA-TB.            COPY 'PARY01TB'.                PAR32A
019300*VALUE CLAUSE IS ALLOWED IN THE LINKAGE SECTION.                  PAR32A
019400*COPY 'PARY01DC' IS LOADED IN WITHOUT THE VALUE CLAUSE.           PAR32A
019500******************************************************************PAR32A
019600*                                                                *PAR32A
019700*                P A R Y 0 1 T B                                 *PAR32A
019800*                                                                *PAR32A
019900*    THIS MEMBER DEFINES THE TABLING AREA WHERE EMPLOYEE DATA IS *PAR32A
020000*    TABLED BY THE PAR EXTRACTION PROGRAMS FOR EXAMINATION.      *PAR32A
020100*    THIS IS A COPY MEMBER BECAUSE IT IS USED IN MULTIPLE        *PAR32A
020200*    PROGRAMS.                                                   *PAR32A
020300*                                                                *PAR32A
020400******************************************************************PAR32A
020500 01  EMPLOYEE-DATA-TB.                                            PAR32A
020600     05  CAMPUS-CODE-TB           PIC 9(2).                       PAR32A
020700     05  EMP-TITLE-CODE-TB        PIC 9(4).                       PAR32A
020800     05  EMP-SSN-TB               PIC 9(9).                       PAR32A
020900     05  EMP-NUMBER-TB            PIC 9(9).                       PAR32A
021000     05  EMP-NAME-TB              PIC X(30).                      PAR32A
021100     05  EMP-HOME-DEPT-TB         PIC 9(6).                       PAR32A
021200     05  TYP-EMP-TB               PIC X.                          PAR32A
021300         88  NINE-MO-FACULTY      VALUE 'N'.                      PAR32A
021400         88  BI-WEEKLY-PAY        VALUE 'B'.                      PAR32A
021500         88  STAFF                VALUE 'S'.                      PAR32A
021600         88  PROFESSIONAL         VALUE 'P'.                      PAR32A
021700     05  EMP-SEL-RSN-TB           PIC XX.                         PAR32A
021800     05  EMP-DIST-DEPT-TB         PIC 9(6).                       PAR32A
021900         05  EMP-TYP-PAR-TB       PIC X.                          PAR32A
022000             88  NON-PROF-TB      VALUE '2'.                      PAR32A
022100             88  PROF-TB          VALUE '1'.                      PAR32A
022200             88  NINE-MO-FAC-TB   VALUE 'N'.                      PAR32A
022300*    05  MAX-ERRORS-TB            PIC S9(4) COMP VALUE +2.        PAR32A
022400     05  MAX-ERRORS-TB            PIC S9(4) COMP.                 PAR32A
022500     05  NUM-ERRORS-TB            PIC S9(4) COMP.                 PAR32A
022600     05  ERROR-CODES-TB           OCCURS 2 TIMES                  PAR32A
022700                                  INDEXED BY ERR-IX1              PAR32A
022800                                             ERR-IX2              PAR32A
022900                                  PIC 99.                         PAR32A
023000     05  EMP-TOTAL-PAY-TB         PIC S9(7)V99 COMP-3.            PAR32A
023100     05  EMP-FLAG-AREA-TB.                                        PAR32A
023200         10  DIRECT-CHARGE-TB     PIC X.                          PAR32A
023300             88  DIRECT-CHARGE    VALUE 'Y'.                      PAR32A
023400         10  ACADEM-DEPT-TB       PIC X.                          PAR32A
023500             88  ACADEM-DEPT      VALUE 'Y'.                      PAR32A
023600         10  TITL-CD-0800-9999-TB  PIC X.                         PAR32A
023700             88  ANY-TITL-CD       VALUE 'Y'.                     PAR32A
023800         10  NINE-MO-FACT-TB      PIC X.                          PAR32A
023900             88  NINE-MO-FAC      VALUE 'Y'.                      PAR32A
024000         10  SYSTEM-WIDE-TB       PIC X.                          PAR32A
024100             88  SYSTEM-WIDE      VALUE '-'.                      PAR32A
024200*    05  ACCT-MAX-ENTRS-TB        PIC S9(4) COMP VALUE +90.       PAR32A
024300     05  ACCT-MAX-ENTRS-TB        PIC S9(4) COMP.                 PAR32A
024400     05  ACCT-NUM-ENTRS-TB        PIC S9(4) COMP.                 PAR32A
024500*    05  ACCT-TABLE-TB            OCCURS 90 TIMES                 PAR32A
024600     05  ACCT-TB-LEVEL.                                           PAR32A
024700     06  ACCT-TABLE-TB            OCCURS 90 TIMES                 PAR32A
024800                                  INDEXED BY ACCT-IX1             PAR32A
024900                                             ACCT-IX2             PAR32A
025000                                             ACCT-IX3.            PAR32A
025100         10  ACCT-PROCESS-TB      PIC X.                          PAR32A
025200             88  ACCT-BYPASS      VALUE 'B'.                      PAR32A
025300             88  ACCT-GOOD        VALUE 'G'.                      PAR32A
025400             88  ACCT-COMBINED    VALUE 'C'.                      PAR32A
025500         10  TITL-NINE-MONTH-TB   PIC X.                          PAR32A
025600             88  TITL-NINE-MONTH  VALUE 'Y'.                      PAR32A
025700         10  TITLE-CD-TB          PIC 9(4).                       PAR32A
025800         10  ACCT-DATE-TB         PIC 9(4).                       PAR32A
025900         10  ACCT-NUMBER-TB       PIC 9(6).                       PAR32A
026000*        10  ACCT-NUMBER-R-TB     REDEFINES ACCT-NUMBER-TB.       PAR32A
026100*            15  ACCT-NUM12-TB    PIC XX.                         PAR32A
026200*            15  ACCT-NUM34-TB    PIC XX.                         PAR32A
026300*            15  ACCT-NUM56-TB    PIC XX.                         PAR32A
026400         10  ACCT-FUND-TB         PIC 9(5).                       PAR32A
026500         10  ACCT-SUBACCT-TB      PIC 9.                          PAR32A
026600         10  ACCT-TYPE-TB         PIC X.                          PAR32A
026700             88  SPON-RES         VALUE '1'.                      PAR32A
026800             88  ALL-OTHER        VALUE '2'.                      PAR32A
026900         10  ACCT-DOLLARS-TB      PIC S9(5)V99 COMP-3.            PAR32A
027000         10  ACCT-SYS-WIDE-TB     PIC X.                          PAR32A
027100             88  ACCT-SYS-WIDE    VALUE '-'.                      PAR32A
027200     05  ACCT-TB-RE-LEVEL REDEFINES ACCT-TB-LEVEL.                PAR32A
027300     06  ACCT-TABLE-TB-RE    OCCURS 90 TIMES.                     PAR32A
027400         10  FILLER               PIC X(10).                      PAR32A
027500         10  ACCT-NUMBER-R-TB.                                    PAR32A
027600             15  ACCT-NUM12-TB    PIC XX.                         PAR32A
027700             15  ACCT-NUM34-TB    PIC XX.                         PAR32A
027800             15  ACCT-NUM56-TB    PIC XX.                         PAR32A
027900         10  FILLER               PIC X(12).                      PAR32A
027910     05  FILLER                   PIC X(03).                      PAR32A
028000 EJECT                                                            PAR32A
028100******************************************************************PAR32A
028200*                                                                *PAR32A
028300*    THE PROCEDURE DIVISION IS ORGANIZED INTO FOUR MAIN SECTIONS.*PAR32A
028400*    0000-MAINLINE                                               *PAR32A
028500*        ENTRY POINT OF PROGRAM                                  *PAR32A
028600*        EXAMINE TYPE OF CALL AND PERFORM PROPER SECTION         *PAR32A
028700*        RETURN                                                  *PAR32A
028800*    1000-INITIALIZATION                                         *PAR32A
028900*        OPENS OUTPUT FILE                                       *PAR32A
029000*        INITIALIZES HEADING LINES                               *PAR32A
029100*        PRINTS FIRST SET OF HEADINGS                            *PAR32A
029200*    5000-PROCESS                                                *PAR32A
029300*        BUILDS AND PRINTS DETAIL LINES                          *PAR32A
029400*    9000-TERMINATION                                            *PAR32A
029500*        CLOSES OUTPUT FILE                                      *PAR32A
029600*        DISPLAYS PROCESSING STATISTICS                          *PAR32A
029700*                                                                *PAR32A
029800******************************************************************PAR32A
029900 PROCEDURE DIVISION USING MODULE-COMMON-AREA                      PAR32A
030000                          EMPLOYEE-DATA-TB.                       PAR32A
030100 0000-MAINLINE SECTION.                                           PAR32A
030200 0000-MAIN-PARA.                                                  PAR32A
030300     MOVE +2    TO   MAX-ERRORS-TB.                               PAR32A
030400     MOVE +90   TO   ACCT-MAX-ENTRS-TB.                           PAR32A
030500     IF PROCESS                                                   PAR32A
030600         PERFORM 5000-PROCESS                                     PAR32A
030700     ELSE                                                         PAR32A
030800         IF INITIALIZATION                                        PAR32A
030900             PERFORM 1000-INITIALIZATION                          PAR32A
031000         ELSE                                                     PAR32A
031100             IF TERMINATION                                       PAR32A
031200                 PERFORM 9000-TERMINATION                         PAR32A
031300             ELSE                                                 PAR32A
031400                 DISPLAY MOD-NAME-C ' INVALID TYPE OF CALL '      PAR32A
031500                         'RECEIVED  VALUE = ' TYPE-OF-CALL-LS     PAR32A
031600                 GO TO 0010-SET-BAD-RETURN.                       PAR32A
031700     GO TO 0020-SET-GOOD-RETURN.                                  PAR32A
031800 0010-SET-BAD-RETURN.                                             PAR32A
031900     MOVE +16 TO RETURN-CODE.                                     PAR32A
032000     GO TO 0030-RETURN.                                           PAR32A
032100 0020-SET-GOOD-RETURN.                                            PAR32A
032200     MOVE +0 TO RETURN-CODE.                                      PAR32A
032300 0030-RETURN.                                                     PAR32A
032400     EXIT PROGRAM.                                                PAR32A
032500 EJECT                                                            PAR32A
032600 1000-INITIALIZATION SECTION.                                     PAR32A
032700 1000-INIT-PARA.                                                  PAR32A
032800     DISPLAY MOD-NAME-C ' INITIALIZED'.                           PAR32A
032900     OPEN OUTPUT EXTRACT-REPORT-OUT.                              PAR32A
033000     MOVE RUN-DATE-MM-LS TO MONTH-H2.                             PAR32A
033100     MOVE RUN-DATE-DD-LS TO DAY-H2.                               PAR32A
033200     MOVE RUN-DATE-YY-LS TO YEAR-H2.                              PAR32A
033300     PERFORM 8000-PRINT-HEADINGS.                                 PAR32A
033400 1001-EXIT-INITIALIZATION.                                        PAR32A
033500     EXIT.                                                        PAR32A
033600 EJECT                                                            PAR32A
033700 5000-PROCESS SECTION.                                            PAR32A
033800 5000-RPOC-PARA.                                                  PAR32A
033900      ADD +1 TO NUM-EMP-PRINTED-W.                                PAR32A
034000     PERFORM 6000-INIT-EMP-LINE.                                  PAR32A
034100     SET ERR-IX1 TO +1.                                           PAR32A
034200     IF ACCT-NUM-ENTRS-TB > +0                                    PAR32A
034300         PERFORM 7000-FORMAT-ACCT                                 PAR32A
034400             VARYING ACCT-IX1 FROM +1 BY +1                       PAR32A
034500             UNTIL ACCT-IX1 > ACCT-NUM-ENTRS-TB                   PAR32A
034600     ELSE                                                         PAR32A
034700         PERFORM 8500-ADD-ERR-MSG                                 PAR32A
034800         PERFORM 7500-PRINT-LINE.                                 PAR32A
034900     MOVE EMP-DIST-DEPT-TB TO DIST-ACCT-D2.                       PAR32A
035000     MOVE DETAIL-2 TO EXTRACT-PRINT-LINE.                         PAR32A
035100     MOVE 1 TO CC-PL.                                             PAR32A
035200     PERFORM 7500-PRINT-LINE.                                     PAR32A
035300 5001-EXIT-PROCESS.                                               PAR32A
035400     EXIT.                                                        PAR32A
035500 EJECT                                                            PAR32A
035600 6000-INIT-EMP-LINE SECTION.                                      PAR32A
035700 6000-INIT-EMP-PARA.                                              PAR32A
035800*    MOVE '0' TO CC-D1.                                           PAR32A
035900     MOVE 2 TO CC-D1.                                             PAR32A
036000     MOVE CAMPUS-CODE-TB TO CAMPUS-D1.                            PAR32A
036100     MOVE EMP-HOME-DEPT-TB TO HOME-DEPT-D1.                       PAR32A
036200     MOVE EMP-NUMBER-TB TO EMP-NO-D1.                             PAR32A
036300     MOVE EMP-NAME-TB TO EMP-NAME-D1.                             PAR32A
036400     MOVE EMP-SEL-RSN-TB TO SEL-RSN-D1.                           PAR32A
036500     MOVE EMP-TYP-PAR-TB TO TYP-PAR-D1.                           PAR32A
036600 6001-EXIT-INIT-EMP-LINE.                                         PAR32A
036700     EXIT.                                                        PAR32A
036800 7000-FORMAT-ACCT SECTION.                                        PAR32A
036900 7000-FORMAT-ACCT-PARA.                                           PAR32A
037000     IF ERR-IX1 > NUM-ERRORS-TB                                   PAR32A
037100         NEXT SENTENCE                                            PAR32A
037200     ELSE                                                         PAR32A
037300         PERFORM 8500-ADD-ERR-MSG                                 PAR32A
037400         SET ERR-IX1 UP BY +1.                                    PAR32A
037500     MOVE TITLE-CD-TB (ACCT-IX1) TO TITL-D1.                      PAR32A
037600     MOVE ACCT-NUMBER-TB (ACCT-IX1) TO ACCT-D1.                   PAR32A
037700     MOVE ACCT-FUND-TB (ACCT-IX1) TO FUND-D1.                     PAR32A
037800     MOVE ACCT-DATE-TB (ACCT-IX1) TO DATE-D1.                     PAR32A
037900     IF ACCT-GOOD (ACCT-IX1)                                      PAR32A
038000         MOVE ACCT-TYPE-TB (ACCT-IX1) TO ACCT-TYPE-D1             PAR32A
038100         IF EMP-TOTAL-PAY-TB > +0.00                              PAR32A
038200             COMPUTE PCT-D1 = (ACCT-DOLLARS-TB (ACCT-IX1) /       PAR32A
038300                              EMP-TOTAL-PAY-TB) * 100             PAR32A
038400         ELSE                                                     PAR32A
038500             MOVE ZERO TO PCT-D1                                  PAR32A
038600     ELSE                                                         PAR32A
038700         MOVE ZERO TO PCT-D1                                      PAR32A
038800         MOVE ACCT-PROCESS-TB (ACCT-IX1) TO ACCT-TYPE-D1.         PAR32A
038900     MOVE ACCT-DOLLARS-TB (ACCT-IX1) TO DOLLARS-D1.               PAR32A
039000     IF NUM-LINES-PAGE-W > MAX-LINES-PAGE-W                       PAR32A
039100         PERFORM 8000-PRINT-HEADINGS                              PAR32A
039200         PERFORM 6000-INIT-EMP-LINE.                              PAR32A
039300     MOVE DETAIL-1 TO EXTRACT-PRINT-LINE.                         PAR32A
039400     PERFORM 7500-PRINT-LINE.                                     PAR32A
039500     MOVE SPACES TO DETAIL-1.                                     PAR32A
039600     MOVE 1 TO CC-D1.                                             PAR32A
039700*                                                                 PAR32A
039800 7001-EXIT-FORMAT-ACCT.                                           PAR32A
039900     EXIT.                                                        PAR32A
040000 7500-PRINT-LINE SECTION.                                         PAR32A
040100 7500-PRINT-PARA.                                                 PAR32A
040200     IF CC-PL = 1                                                 PAR32A
040300         ADD +1 TO NUM-LINES-PAGE-W                               PAR32A
040400     ELSE                                                         PAR32A
040500         IF CC-PL = 2                                             PAR32A
040600             ADD +2 TO NUM-LINES-PAGE-W                           PAR32A
040700         ELSE                                                     PAR32A
040800             IF CC-PL = 3                                         PAR32A
040900                 ADD +3 TO NUM-LINES-PAGE-W                       PAR32A
041000             ELSE                                                 PAR32A
041100*                IF CC-PL = 0                                     PAR32A
041200*                    MOVE +1 TO NUM-LINES-PAGE-W                  PAR32A
041300*                ELSE                                             PAR32A
041400                     ADD +1 TO NUM-LINES-PAGE-W.                  PAR32A
041500*    WRITE EXTRACT-PRINT-LINE AFTER POSITIONING CC-PL.            PAR32A
041600     WRITE EXTRACT-PRINT-LINE AFTER CC-PL.                        PAR32A
041700 7501-EXIT-PRINT-LINE.                                            PAR32A
041800     EXIT.                                                        PAR32A
041900 EJECT                                                            PAR32A
042000 8000-PRINT-HEADINGS SECTION.                                     PAR32A
042100 8000-PRINT-HEADINGS-PARA.                                        PAR32A
042200     ADD +1 TO PAGE-NUM-W.                                        PAR32A
042300     MOVE PAGE-NUM-W TO PAGE-NUM-H1.                              PAR32A
042400     MOVE +1 TO NUM-LINES-PAGE-W.                                 PAR32A
042500     MOVE HEADING-H1 TO EXTRACT-PRINT-LINE.                       PAR32A
042510     MOVE HEADING-H1 TO EXTRACT-PRINT-LINE.                       PAR32A
042600*    PERFORM 7500-PRINT-LINE.                                     PAR32A
042700     WRITE EXTRACT-PRINT-LINE AFTER PAGE.                         PAR32A
042800     MOVE HEADING-H2 TO EXTRACT-PRINT-LINE.                       PAR32A
042900     PERFORM 7500-PRINT-LINE.                                     PAR32A
043000     MOVE HEADING-H3 TO EXTRACT-PRINT-LINE.                       PAR32A
043100     PERFORM 7500-PRINT-LINE.                                     PAR32A
043200     MOVE HEADING-H4 TO EXTRACT-PRINT-LINE.                       PAR32A
043300     PERFORM 7500-PRINT-LINE.                                     PAR32A
043400 8001-EXIT-PRINT-HEADINGS.                                        PAR32A
043500     EXIT.                                                        PAR32A
043600 8500-ADD-ERR-MSG SECTION.                                        PAR32A
043700 8500-ERR-PARA.                                                   PAR32A
043800     IF ERR-IX1 = +1                                              PAR32A
043900         ADD +1 TO NUM-EMP-ERROR-W.                               PAR32A
044000     MOVE ERROR-CODES-TB (ERR-IX1) TO EXC-CD-D1.                  PAR32A
044100     SET EXC-IX TO ERROR-CODES-TB (ERR-IX1).                      PAR32A
044200     MOVE EXC-DESC-W (EXC-IX) TO EXC-DESC-D1.                     PAR32A
044300 8501-EXIT-ADD-ERR-MSG.                                           PAR32A
044400     EXIT.                                                        PAR32A
044500 EJECT                                                            PAR32A
044600 9000-TERMINATION SECTION.                                        PAR32A
044700 9000-TERM-PARA.                                                  PAR32A
044800     CLOSE EXTRACT-REPORT-OUT.                                    PAR32A
044900     DISPLAY MOD-NAME-C ' PROCESSING TERMINATED'.                 PAR32A
045000     DISPLAY '         NUMBER EMPLOYEES PRINTED = '               PAR32A
045100                                 NUM-EMP-PRINTED-W.               PAR32A
045200     DISPLAY '         NUMBER EMPLOYEES ERRORED = '               PAR32A
045300                                 NUM-EMP-ERROR-W.                 PAR32A
045400 9001-EXIT-TERMINATION.                                           PAR32A
045500     EXIT.                                                        PAR32A
