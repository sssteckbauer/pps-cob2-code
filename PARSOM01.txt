000373 IDENTIFICATION DIVISION.                                         PARSOM01
000374 PROGRAM-ID.  PARSOM01.                                           PARSOM01
000380 DATE-WRITTEN.  JANUARY 1986.                                     PARSOM01
000400 DATE-COMPILED.                                                   PARSOM01
000500*REMARKS.                                                         PARSOM01
000600******************************************************************PARSOM01
000700*                                                                *PARSOM01
000800*FUNCTION:                                                       *PARSOM01
000900*   THIS PROGRAM READS THE PAR HISTORY FILE AND EXTRACTS         *PARSOM01
001000*        ALL MEDICAL SCHOOL EMPLOYEES                            *PARSOM01
001100*             DEPT = 03XX                                         PARSOM01
001200*                    12XX                                        *PARSOM01
001300*                    13XX                                        *PARSOM01
001400*         OTHER EMPLOYEES WHO HAVE BEEN PAYED FROM ACCOUNTS      *PARSOM01
001500*             ACCT = 4048XX                                       PARSOM01
001600*                    4049XX                                      *PARSOM01
001700*                    4047XX                                      *PARSOM01
001800*         THE RECORDS ARE FLAGGED AS                             *PARSOM01
001900*             STAFF = S   MEDICAL SCHOOL STAFF                   *PARSOM01
002000*             FALCULTY = F  "      "     FALCULTY                *PARSOM01
002100*             OTHER    = X  NOT SOM DEPT BUT FROM SOM ACCT       *PARSOM01
002200*                                                                *PARSOM01
002300*  CONTROL CARD ADDED                                            *PARSOM01
002400*  LOW YEAR TWO CHARACTER                                        *PARSOM01
002500*  HIGH YEAR TWO CHARACTER                                       *PARSOM01
002600******************************************************************PARSOM01
002601*  CONVERTED TO IBM                                              *PARSOM01
002602*     PERVIOUS NAME: SOMEXT                                      *PARSOM01
002603*     NEW NAME:      PARSOM01                                    *PARSOM01
002604*     PROGRAM NOW READS CUMMULATIVE PAR FILE TO EXTRACT DATA.    *PARSOM01
002605*     NO ACCESS TO EDB IS NEEDED SINCE DEPARTMENT IS ON PAR FILE.*PARSOM01
002606*                                                                *PARSOM01
002610******************************************************************PARSOM01
002700*                                                                 PARSOM01
002800 ENVIRONMENT DIVISION.                                            PARSOM01
002900 INPUT-OUTPUT SECTION.                                            PARSOM01
003000 FILE-CONTROL.                                                    PARSOM01
003100                                                                  PARSOM01
003800*                                                                 PARSOM01
003810     SELECT EXTRACT-SOM-OUT       ASSIGN TO UT-S-SOMEXT.          PARSOM01
003820     SELECT PAYROLL-HISTORY-IN    COPY  CPSLXPAR .                PARSOM01
003830     SELECT SPEC-CARD                                             PARSOM01
003840         COPY CPSLXCRD.                                           PARSOM01
003850                                                                  PARSOM01
003900 DATA DIVISION.                                                   PARSOM01
004000 FILE SECTION.                                                    PARSOM01
004100 FD  EXTRACT-SOM-OUT                                              PARSOM01
004210     RECORDING MODE IS F                                          PARSOM01
004900     RECORD CONTAINS 73 CHARACTERS                                PARSOM01
004230     BLOCK CONTAINS 0 RECORDS                                     PARSOM01
004240     LABEL RECORDS ARE STANDARD                                   PARSOM01
004250     DATA RECORD IS                                               PARSOM01
004260     EXTRACT-REC.                                                 PARSOM01
004400*                                                                 PARSOM01
004500 01  EXTRACT-REC.                                                 PARSOM01
004600     03  EMPLNO         PIC 9(9).                                 PARSOM01
004700     03  NAME           PIC X(30).                                PARSOM01
004800     03  DEPT           PIC 9(04).                                PARSOM01
004900     03  TITLE-CODE     PIC 9(4).                                 PARSOM01
005000     03  ACCOUNT        PIC 9(6).                                 PARSOM01
005100     03  FUND           PIC 9(5).                                 PARSOM01
005200     03  SUB            PIC 9(1).                                 PARSOM01
005300     03  GROSSPAY       PIC S9(5)V9(2).                           PARSOM01
005400     03  F-S-X           PIC X.                                   PARSOM01
005500     03  ENDDATE.                                                 PARSOM01
005600         05  MO         PIC 9(2).                                 PARSOM01
005700         05  DA         PIC 9(2).                                 PARSOM01
005800         05  YY         PIC 9(2).                                 PARSOM01
005900*                                                                 PARSOM01
006000                                                                  PARSOM01
006100*                                                                 PARSOM01
006110 FD  PAYROLL-HISTORY-IN           COPY  CPFDXPAR .                PARSOM01
006120*                                                                 PARSOM01
006130 01  PAYROLL-MASTER-XPAR.         COPY  CPWSXPAR .                PARSOM01
006140                                                                  PARSOM01
006150 EJECT                                                            PARSOM01
006195 FD  SPEC-CARD                                                    PARSOM01
006196         COPY CPFDXCRD.                                           PARSOM01
006197*                                                                 PARSOM01
006198 01  SPEC-CARD-REC               PIC X(80).                       PARSOM01
006199*                                                                 PARSOM01
006200 EJECT                                                            PARSOM01
007100 WORKING-STORAGE SECTION.                                         PARSOM01
007900                                                                  PARSOM01
008500 01  MISC-COUNTER-AREA.                                           PARSOM01
008600     05  REC-COUNTER             PIC 9(9) VALUE 0.                PARSOM01
008700     05  REC-WRIT                PIC 9(6) VALUE 0.                PARSOM01
008800     05  PAY-CUR-EOF             PIC 9(01) VALUE 0.               PARSOM01
008900     05  PROCESS-COMPLETE-SW     PIC 9(01) VALUE 0.               PARSOM01
009000     05  PCR-COMPLETE-SW         PIC 9(01) VALUE 0.               PARSOM01
009100     05  ACCT-IX                 PIC S9(9) COMP SYNC VALUE +0.    PARSOM01
009200     05  NAME-WK                 PIC X(30).                       PARSOM01
009300     05  FILLER                   REDEFINES NAME-WK.              PARSOM01
009400         10  PCR-NAME-WK          PIC X(3).                       PARSOM01
009500         10  FILLER               PIC X(27).                      PARSOM01
009600                                                                  PARSOM01
008000 01  INPUT-PARAM.                                                 PARSOM01
008100     05  IN-LOW-YY               PIC XX.                          PARSOM01
008200     05  IN-HIGH-YY              PIC XX.                          PARSOM01
008210     05  FILLER                  PIC X(76).                       PARSOM01
008300                                                                  PARSOM01
008400 01  DEPT-AREA.                                                   PARSOM01
008500     05  FILLER                  PIC X(02).                       PARSOM01
008510     05  DEPT1-PART              PIC 9(02).                       PARSOM01
008600         88  SOM-DEPT  VALUE 03  13  12.                          PARSOM01
008700     05  DEPT2-PART              PIC 9(02).                       PARSOM01
008800                                                                  PARSOM01
008810 01  DEPT-AREA4 REDEFINES DEPT-AREA.                              PARSOM01
008811     05  FILLER                  PIC X(02).                       PARSOM01
008812     05  DEPT-4DIGIT             PIC 9(04).                       PARSOM01
008820                                                                  PARSOM01
008900 01  ACCT-AREA.                                                   PARSOM01
009000     05  ACCT-PART               PIC 9(04).                       PARSOM01
009100         88 SOM-ACCT     VALUE 4047 4048 4049.                    PARSOM01
009200     05  FILLER                  PIC 9(02).                       PARSOM01
009300                                                                  PARSOM01
009400 01 TITLE-AREA.                                                   PARSOM01
009500    05  TITLE-TEST               PIC 9(04).                       PARSOM01
009600        88  SOM-FACULTY   VALUE  0800 THRU 3999.                  PARSOM01
009700                                                                  PARSOM01
009800 01 DATE-AREA.                                                    PARSOM01
009900    05  HIGH-YEAR                PIC 99.                          PARSOM01
010000    05  LOW-YEAR                 PIC 99.                          PARSOM01
010100    05  TEST-MO                  PIC 99.                          PARSOM01
010200        88  LOW-MO        VALUE 1 2 3 4 5 6.                      PARSOM01
010300        88  HIGH-MO       VALUE 7 8 9 10 11 12.                   PARSOM01
010400                                                                  PARSOM01
010500*                                                                 PARSOM01
010600 PROCEDURE DIVISION.                                              PARSOM01
010700 0000-MAIN-PROC SECTION.                                          PARSOM01
010800     PERFORM 1000-ACCEPT-PARAMATERS.                              PARSOM01
010900     OPEN INPUT PAYROLL-HISTORY-IN.                               PARSOM01
011100     OPEN OUTPUT EXTRACT-SOM-OUT.                                 PARSOM01
011200     MOVE 0 TO PAY-CUR-EOF.                                       PARSOM01
011210     PERFORM 6000-READ-PAY-HISTORY THRU                           PARSOM01
011220         6001-EXIT-READ-PAY-HISTORY.                              PARSOM01
011300     PERFORM 2000-PROCESS-INPUT                                   PARSOM01
011400         THRU 2000-PROCESS-INPUT-EXIT                             PARSOM01
011500         UNTIL PROCESS-COMPLETE-SW = 1.                           PARSOM01
011600                                                                  PARSOM01
011700     CLOSE EXTRACT-SOM-OUT, PAYROLL-HISTORY-IN.                   PARSOM01
011900     DISPLAY ' TOTAL RECORDS =  ' REC-COUNTER.                    PARSOM01
012000         DISPLAY 'RECORDS WRITTEN = ' REC-WRIT.                   PARSOM01
012110     GO TO 0020-SET-GOOD-RETURN.                                  PARSOM01
012111                                                                  PARSOM01
012120 0010-SET-BAD-RETURN.                                             PARSOM01
012130     MOVE +16 TO RETURN-CODE.                                     PARSOM01
012140     GO TO 0030-RETURN.                                           PARSOM01
012141                                                                  PARSOM01
012150 0020-SET-GOOD-RETURN.                                            PARSOM01
012160     MOVE +0 TO RETURN-CODE.                                      PARSOM01
012161                                                                  PARSOM01
012170 0030-RETURN.                                                     PARSOM01
012171     STOP RUN.                                                    PARSOM01
012200                                                                  PARSOM01
015600 1000-ACCEPT-PARAMATERS SECTION.                                  PARSOM01
012310     OPEN INPUT SPEC-CARD.                                        PARSOM01
015800     READ SPEC-CARD INTO INPUT-PARAM                              PARSOM01
012330          AT END                                                  PARSOM01
012340         GO TO 0010-SET-BAD-RETURN.                               PARSOM01
012350     CLOSE SPEC-CARD.                                             PARSOM01
012500     IF IN-LOW-YY NOT NUMERIC                                     PARSOM01
012600         DISPLAY 'LOW YEAR IS NOT NUMERIC' IN-LOW-YY              PARSOM01
012700         DISPLAY 'PLEASE FIX AND RESTART'                         PARSOM01
012800         STOP RUN.                                                PARSOM01
013000     IF IN-HIGH-YY NOT NUMERIC                                    PARSOM01
013100         DISPLAY 'HIGH YEAR IS NOT NUMERIC' IN-HIGH-YY            PARSOM01
013200         DISPLAY 'PLEASE FIX AND RESTART'                         PARSOM01
013300         STOP RUN.                                                PARSOM01
013400     MOVE IN-LOW-YY TO LOW-YEAR.                                  PARSOM01
013500     MOVE IN-HIGH-YY TO HIGH-YEAR.                                PARSOM01
013600     DISPLAY 'LOW-YEAR IS ' IN-LOW-YY.                            PARSOM01
013700     DISPLAY 'HIGH-YEAR IS ' IN-HIGH-YY.                          PARSOM01
013800                                                                  PARSOM01
013810 1000-ACCCEPT-PARAMATERS-EXIT.                                    PARSOM01
013820     EXIT.                                                        PARSOM01
013830                                                                  PARSOM01
013900 2000-PROCESS-INPUT SECTION.                                      PARSOM01
014620     IF PAY-CUR-EOF = 1                                           PARSOM01
014630         MOVE 1 TO PROCESS-COMPLETE-SW                            PARSOM01
014640         GO TO 2000-PROCESS-INPUT-EXIT.                           PARSOM01
014650     PERFORM 3000-EXAMINE-ACCOUNTS                                PARSOM01
014660         VARYING ACCT-IX FROM +1 BY +1                            PARSOM01
014670         UNTIL ACCT-IX > XPAR-NO-ACCTS.                           PARSOM01
014680     IF PAY-CUR-EOF = 1                                           PARSOM01
014690         MOVE 1 TO PROCESS-COMPLETE-SW                            PARSOM01
014691     ELSE                                                         PARSOM01
014692         PERFORM 6000-READ-PAY-HISTORY THRU                       PARSOM01
014693             6001-EXIT-READ-PAY-HISTORY.                          PARSOM01
014694                                                                  PARSOM01
014695 2000-PROCESS-INPUT-EXIT.                                         PARSOM01
014696     EXIT.                                                        PARSOM01
014698                                                                  PARSOM01
014699 3000-EXAMINE-ACCOUNTS SECTION.                                   PARSOM01
014710                                                                  PARSOM01
014712     MOVE XPAR-PER-END-MO (ACCT-IX) TO TEST-MO.                   PARSOM01
014713     IF XPAR-PER-END-YR (ACCT-IX) = HIGH-YEAR                     PARSOM01
014714         AND LOW-MO                                               PARSOM01
014716         NEXT SENTENCE                                            PARSOM01
014717     ELSE                                                         PARSOM01
014718     IF XPAR-PER-END-YR (ACCT-IX) = LOW-YEAR                      PARSOM01
014720         AND HIGH-MO                                              PARSOM01
014721         NEXT SENTENCE                                            PARSOM01
014722     ELSE                                                         PARSOM01
020500         GO TO 3000-EXIT-EXAMINE-ACCOUNTS.                        PARSOM01
014724                                                                  PARSOM01
014725     MOVE XPAR-DEPT TO DEPT-AREA.                                 PARSOM01
020800     MOVE XPAR-TTL-CD (ACCT-IX) TO TITLE-TEST.                    PARSOM01
014727     IF SOM-DEPT                                                  PARSOM01
014728         PERFORM 4000-FIND-F-S                                    PARSOM01
014729         PERFORM 7000-WRITE-OUTPUT                                PARSOM01
021200         GO TO 3000-EXIT-EXAMINE-ACCOUNTS.                        PARSOM01
014740                                                                  PARSOM01
014753     MOVE XPAR-ACCT (ACCT-IX) TO ACCT-AREA.                       PARSOM01
014754     IF SOM-ACCT                                                  PARSOM01
014755          MOVE 'X' TO F-S-X                                       PARSOM01
014756          PERFORM 7000-WRITE-OUTPUT.                              PARSOM01
014757                                                                  PARSOM01
014763 3000-EXIT-EXAMINE-ACCOUNTS.                                      PARSOM01
014764     EXIT.                                                        PARSOM01
016600                                                                  PARSOM01
016800 4000-FIND-F-S SECTION.                                           PARSOM01
017000     IF SOM-FACULTY                                               PARSOM01
017100          MOVE 'F' TO F-S-X                                       PARSOM01
017200     ELSE                                                         PARSOM01
017300          MOVE 'S' TO F-S-X.                                      PARSOM01
017301                                                                  PARSOM01
017302 4000-FIND-F-S-EXIT.                                              PARSOM01
017303     EXIT.                                                        PARSOM01
017304                                                                  PARSOM01
017310 6000-READ-PAY-HISTORY SECTION.                                   PARSOM01
017320     IF PAY-CUR-EOF = 1                                           PARSOM01
017330         MOVE 1 TO PROCESS-COMPLETE-SW                            PARSOM01
023400         DISPLAY ' PAY CURRENT EOF ON ATTEMPT READ'               PARSOM01
017350         GO TO 6001-EXIT-READ-PAY-HISTORY.                        PARSOM01
017360     PERFORM 6001-READPAY                                         PARSOM01
017370        THRU 6001-READPAY-EX.                                     PARSOM01
017380     IF  PAY-CUR-EOF = 1                                          PARSOM01
017390         MOVE 1 TO PROCESS-COMPLETE-SW                            PARSOM01
017391             GO TO 6001-EXIT-READ-PAY-HISTORY.                    PARSOM01
017392     MOVE 0 TO PCR-COMPLETE-SW.                                   PARSOM01
017393     PERFORM 6002-EAT-CNTL-RCDS                                   PARSOM01
017394        THRU 6002-EXIT-EAT-CNTL-RCDS                              PARSOM01
017395         UNTIL PCR-COMPLETE-SW = 1.                               PARSOM01
017396                                                                  PARSOM01
017400 6001-EXIT-READ-PAY-HISTORY.                                      PARSOM01
017401     EXIT.                                                        PARSOM01
017402                                                                  PARSOM01
017403 6001-READPAY SECTION.                                            PARSOM01
017404     IF  PAY-CUR-EOF = 1                                          PARSOM01
017405         MOVE 1 TO PROCESS-COMPLETE-SW                            PARSOM01
017406             GO TO 6001-READPAY-EX.                               PARSOM01
017407     READ PAYROLL-HISTORY-IN                                      PARSOM01
017408         AT END                                                   PARSOM01
017409             MOVE '1' TO PAY-CUR-EOF                              PARSOM01
017410             MOVE '1' TO PROCESS-COMPLETE-SW.                     PARSOM01
017411     ADD +1 TO REC-COUNTER.                                       PARSOM01
017412                                                                  PARSOM01
017413 6001-READPAY-EX.                                                 PARSOM01
017414      EXIT.                                                       PARSOM01
017415                                                                  PARSOM01
017416 6002-EAT-CNTL-RCDS SECTION.                                      PARSOM01
017417     IF PAY-CUR-EOF = 1                                           PARSOM01
017418         MOVE 1 TO PCR-COMPLETE-SW                                PARSOM01
017419         GO TO 6002-EXIT-EAT-CNTL-RCDS.                           PARSOM01
017420     MOVE XPAR-NAME TO NAME-WK.                                   PARSOM01
017421     IF PCR-NAME-WK = 'PCR'                                       PARSOM01
017423         PERFORM 6001-READPAY                                     PARSOM01
017424            THRU 6001-READPAY-EX                                  PARSOM01
017425     ELSE                                                         PARSOM01
017426         MOVE 1 TO PCR-COMPLETE-SW                                PARSOM01
017427         GO TO 6002-EXIT-EAT-CNTL-RCDS.                           PARSOM01
017428                                                                  PARSOM01
017429 6002-EXIT-EAT-CNTL-RCDS.                                         PARSOM01
017430     EXIT.                                                        PARSOM01
020510                                                                  PARSOM01
020600 7000-WRITE-OUTPUT SECTION.                                       PARSOM01
020740     MOVE XPAR-ID-NO                TO EMPLNO.                    PARSOM01
020750     MOVE XPAR-NAME                 TO NAME.                      PARSOM01
028000     MOVE DEPT-4DIGIT               TO DEPT.                      PARSOM01
028100     MOVE XPAR-TTL-CD (ACCT-IX)     TO TITLE-CODE.                PARSOM01
020780     MOVE XPAR-ACCT (ACCT-IX)       TO ACCOUNT.                   PARSOM01
020790     MOVE XPAR-FUND (ACCT-IX)       TO FUND.                      PARSOM01
020791     MOVE XPAR-AF-SUB (ACCT-IX)     TO SUB.                       PARSOM01
020792     MOVE XPAR-EARN-AMT (ACCT-IX)   TO GROSSPAY.                  PARSOM01
021200     MOVE XPAR-PER-END-MO (ACCT-IX) TO MO.                        PARSOM01
021210     MOVE XPAR-PER-END-DA (ACCT-IX) TO DA.                        PARSOM01
021220     MOVE XPAR-PER-END-YR (ACCT-IX) TO YY.                        PARSOM01
021800     WRITE EXTRACT-REC.                                           PARSOM01
021900     ADD +1 TO REC-WRIT.                                          PARSOM01
022000                                                                  PARSOM01
022100 7000-WRITE-OUTPUT-EXIT.                                          PARSOM01
022200     EXIT.                                                        PARSOM01
